package org.orchid.update_constraint;

import org.orchid.models.Constraints;
import org.orchid.models.Role;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

@WebServlet(name = "UpdateConstraintServlet")
public class UpdateConstraintServlet extends HttpServlet {
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String email = request.getParameter("email");
        String roleId = request.getParameter("roleId");
        String[] constraints = request.getParameterValues("constraint");
        UpdateConstraintManager manager = new UpdateConstraintManager();
        manager.getLogin().setEmail(email);

        Role role = new Role(Integer.parseInt(roleId));
        for (String c : constraints) {
            role.getConstraint().add(new Constraints(Integer.parseInt(c)));
        }
        Set<Role> roleSet = new HashSet<Role>();
        roleSet.add(role);
        manager.getLogin().getPerson().setRole(roleSet);
        manager.updateConstraint();
    }

}
