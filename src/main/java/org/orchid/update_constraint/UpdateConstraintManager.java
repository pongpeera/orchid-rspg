package org.orchid.update_constraint;


import org.hibernate.Session;
import org.orchid.models.Constraints;
import org.orchid.models.Login;
import org.orchid.models.Role;
import org.orchid.utils.HibernateUtil;

import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

public class UpdateConstraintManager {
    public Login login;

    public UpdateConstraintManager () {
        this.login = new Login();
    }

    public Login getLogin() {
        return login;
    }

    public void setLogin(Login login) {
        this.login = login;
    }

    public boolean updateConstraint () {
        Session hibernateSession = HibernateUtil.getSession();
        try {
            for (Role r : this.getLogin().getPerson().getRole()) {
                StoredProcedureQuery deleteQuery = hibernateSession.createStoredProcedureQuery("p_deleteConstraint")
                        .registerStoredProcedureParameter(1,Integer.class, ParameterMode.IN)
                        .registerStoredProcedureParameter(2,String.class, ParameterMode.IN);
                deleteQuery.setParameter(1,r.getRoleId());
                deleteQuery.setParameter(2,this.login.getEmail());
                deleteQuery.execute();

                for (Constraints c : r.getConstraint()) {
                    StoredProcedureQuery constQuery = hibernateSession.createStoredProcedureQuery("p_updateConstraint")
                            .registerStoredProcedureParameter(1,Integer.class, ParameterMode.IN)
                            .registerStoredProcedureParameter(2,Integer.class, ParameterMode.IN)
                            .registerStoredProcedureParameter(3,String.class, ParameterMode.IN);
                    constQuery.setParameter(1,c.getConstraintId());
                    constQuery.setParameter(2,r.getRoleId());
                    constQuery.setParameter(3,this.login.getEmail());
                    constQuery.execute();

                }

                hibernateSession.beginTransaction().commit();
                hibernateSession.close();
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }

        return false;
    }
}
