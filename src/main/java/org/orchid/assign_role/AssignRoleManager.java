package org.orchid.assign_role;

import org.hibernate.Session;
import org.orchid.models.Constraints;
import org.orchid.models.Login;
import org.orchid.models.Role;
import org.orchid.utils.HibernateUtil;

import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

public class AssignRoleManager {
    public Login login;

    public AssignRoleManager () {
        this.login = new Login();
    }

    public Login getLogin() {
        return login;
    }

    public void setLogin(Login login) {
        this.login = login;
    }

    public boolean assignRole() {
        Session hibernateSession = HibernateUtil.getSession();
        try {
            StoredProcedureQuery deleteQuery = hibernateSession.createStoredProcedureQuery("p_deleteRole")
                    .registerStoredProcedureParameter(1,String.class, ParameterMode.IN);
            deleteQuery.setParameter(1,this.login.getEmail());
            deleteQuery.execute();

            for (Role r : this.login.getPerson().getRole()) {
                StoredProcedureQuery query = hibernateSession.createStoredProcedureQuery("p_assignRole")
                        .registerStoredProcedureParameter(1,Integer.class, ParameterMode.IN)
                        .registerStoredProcedureParameter(2,String.class, ParameterMode.IN);
                query.setParameter(1,r.getRoleId());
                query.setParameter(2,this.login.getEmail());
                query.execute();
            }
            hibernateSession.beginTransaction().commit();
            hibernateSession.close();
        } catch (Exception e) {
            System.out.print("assign role : "+e.getMessage());
        }
        return false;
    }
}
