package org.orchid.assign_role;


import org.json.JSONObject;
import org.orchid.models.Role;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "AssignRoleServlet")
public class AssignRoleServlet extends HttpServlet {
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");

        JSONObject json = new JSONObject();
        String email = request.getParameter("email");
        String[] roleParams = request.getParameterValues("roles");
        AssignRoleManager manager = new AssignRoleManager();
        manager.getLogin().setEmail(email);
        for (String r : roleParams) {
            manager.getLogin().getPerson().getRole().add(new Role(Integer.parseInt(r)));
        }
        boolean result = manager.assignRole();
        json.put("result",result);
        response.getWriter().append(json.toString());
    }
}
