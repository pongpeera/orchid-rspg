package org.orchid.counter.bean;

public class LocationViewBean {
	private int count;
	private String country;

	public LocationViewBean() {

	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
}
