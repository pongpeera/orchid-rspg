package org.orchid.counter.bean;

public class CounterBean {
	private int count;
	private String description;

	public CounterBean() {
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
