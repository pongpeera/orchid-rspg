package org.orchid.counter.bean;

public class ContentViewBean {
	private int count;
	private String uri;

	public ContentViewBean() {
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}
}
