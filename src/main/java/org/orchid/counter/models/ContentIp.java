package org.orchid.counter.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "t_contentip")
public class ContentIp implements Serializable {

	private static final long serialVersionUID = 1L;

	private long id;
	private Content content;
	private IpInfo ipInfo;
	private String userAgent;
	private Date commingTime;
	private String description;

	public ContentIp() {

	}

	public ContentIp(Content content, IpInfo ipInfo) {
		setContent(content);
		setIpInfo(ipInfo);
	}

	@Id
	@GeneratedValue(generator="increment")
	@GenericGenerator(name="increment", strategy = "increment")
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@ManyToOne
	public Content getContent() {
		return content;
	}

	public void setContent(Content content) {
		this.content = content;
	}

	@ManyToOne
	public IpInfo getIpInfo() {
		return ipInfo;
	}

	public void setIpInfo(IpInfo ipInfo) {
		this.ipInfo = ipInfo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public Date getCommingTime() {
		return commingTime;
	}

	public void setCommingTime(Date commingTime) {
		this.commingTime = commingTime;
	}

}
