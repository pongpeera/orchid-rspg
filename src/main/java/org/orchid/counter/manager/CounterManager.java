package org.orchid.counter.manager;

import java.util.Date;
import java.util.List;

import org.orchid.counter.bean.ContentViewBean;
import org.orchid.counter.bean.LocationViewBean;
import org.orchid.counter.models.Content;

public class CounterManager {

	/*
	 * All page views
	 */
	public static int getAllPageViews() {
		return ContentManager.getAllPageViews();
	}

	/*
	 * All vistors
	 */
	public static int getAllVistors() {
		return IpInfoManager.getAllVistors();
	}

	/*
	 * Total vistors per mont
	 */
	public static int getTotalVisitors() {
		return IpInfoManager.getTotalVisitorsPerMonth(new Date());
	}

	/*
	 * New Visits per month
	 */
	public static int getNewVisits() {
		return IpInfoManager.getNewVisitsPerMonth(new Date());
	}

	/*
	 * Return Visits per month
	 */
	public static int getReturnVisits() {
		return IpInfoManager.getReturnVisitsPerMonth(new Date());
	}

	/*
	 * Page Views per month
	 */
	public static int getRageViews() {
		return ContentIpManager.getRageViewsPerMonth(new Date());
	}

	/*
	 * Top Location
	 */
	public static List<LocationViewBean> getTopLocation(int top) {
		return IpInfoManager.getTopLocation(top);
	}

	/*
	 * Top content
	 */
	public static List<ContentViewBean> getTopContent(int top) {
		return ContentManager.getTopContent(top);
	}

	/*
	 * Counter by URI
	 */
	public static int getCountByPage(String uri) {
		Content content = ContentManager.findByUri(uri);
		if (content != null) {
			return content.getCount();
		}
		return 0;
	}

}
