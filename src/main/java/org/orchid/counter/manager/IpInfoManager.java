package org.orchid.counter.manager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.TemporalType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.orchid.counter.bean.LocationViewBean;
import org.orchid.counter.models.IpInfo;
import org.orchid.utils.HibernateUtil;

public class IpInfoManager {

	private static Log log = LogFactory.getLog(IpInfoManager.class);

	public static IpInfo findByIp(String ip) {
		IpInfo ipInfo = null;
		Session session = HibernateUtil.getSession();
		Transaction ta = null;

		try {
			String queryString = "SELECT ipinfo FROM IpInfo as ipinfo WHERE ipinfo.ip = :ip";
			
			ta = session.beginTransaction();
			
			List<?> listResult = session.createQuery(queryString)
					.setParameter("ip", ip)
					.getResultList();
			if (listResult.size() > 0) {
				ipInfo = (IpInfo) listResult.get(0);
			}
			ta.commit();
		} catch (HibernateException ex) {
			if (ta != null) {
				ta.rollback();
			}
		} finally {
			session.close();
		}
		return ipInfo;
	}

	public static void create(IpInfo ipInfo) {
		Session session = HibernateUtil.getSession();
		Transaction ta = null;
		try {
			ta = session.beginTransaction();
			session.saveOrUpdate(ipInfo);
			ta.commit();
		} catch (HibernateException ex) {
			if (ta != null) {
				ta.rollback();
			}
		} finally {
			session.close();
		}
	}

	public static void update(IpInfo ipInfo) {
		Session session = HibernateUtil.getSession();
		Transaction ta = null;
		try {
			ta = session.beginTransaction();
			session.update(ipInfo);
			ta.commit();
		} catch (HibernateException ex) {
			if (ta != null) {
				ta.rollback();
			}
		} finally {
			session.close();
		}
	}

	public static int getAllVistors() {
		int count = 0;
		Session session = HibernateUtil.getSession();
		Transaction ta = null;

		try {
			String queryString = "FROM IpInfo";

			ta = session.beginTransaction();
			List<?> listResult = session.createQuery(queryString).getResultList();
			if (listResult != null) {
				count = listResult.size();
			}
			ta.commit();
		} catch (HibernateException ex) {
			if (ta != null) {
				ta.rollback();
			}
		} finally {
			session.close();
		}
		return count;
	}

	public static int getTotalVisitorsPerMonth(Date date) {
		int count = 0;
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, -1);
		Date lastMonth = cal.getTime();

		Session session = HibernateUtil.getSession();
		Transaction ta = null;

		try {
			String queryString = "FROM IpInfo AS ipinfo WHERE ipinfo.lastAccessTime BETWEEN :stDate AND :edDate ";

			ta = session.beginTransaction();
			List<?> listResult = session.createQuery(queryString)
					.setParameter("stDate", lastMonth, TemporalType.TIMESTAMP)
					.setParameter("edDate", date, TemporalType.TIMESTAMP)
					.getResultList();
			if (listResult != null) {
				count = listResult.size();
			}
			ta.commit();
		} catch (HibernateException ex) {
			if (ta != null) {
				ta.rollback();
			}
		} finally {
			session.close();
		}
		return count;
	}

	public static int getReturnVisitsPerMonth(Date date) {
		int count = 0;
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, -1);
		Date lastMonth = cal.getTime();

		Session session = HibernateUtil.getSession();
		Transaction ta = null;

		try {
			String queryString = "FROM IpInfo AS ipinfo WHERE ipinfo.lastAccessTime BETWEEN :stDate AND :edDate ";

			ta = session.beginTransaction();
			List<?> listResult = session.createQuery(queryString)
					.setParameter("stDate", lastMonth, TemporalType.TIMESTAMP)
					.setParameter("edDate", date, TemporalType.TIMESTAMP)
					.getResultList();
			if (listResult != null) {
				for (Object obj : listResult) {
					if (obj instanceof IpInfo) {
						if (((IpInfo) obj).getCreateTime() != null
								&&
								((IpInfo) obj).getCreateTime()
										.before(lastMonth)) {
							count++;
						}
					}
				}
			}
			ta.commit();
		} catch (HibernateException ex) {
			if (ta != null) {
				ta.rollback();
			}
		} finally {
			session.close();
		}
		return count;
	}

	public static int getNewVisitsPerMonth(Date date) {
		int count = 0;
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, -1);
		Date lastMonth = cal.getTime();

		Session session = HibernateUtil.getSession();
		Transaction ta = null;

		try {
			String queryString = "FROM IpInfo AS ipinfo WHERE ipinfo.lastAccessTime BETWEEN :stDate AND :edDate ";

			ta = session.beginTransaction();
			List<?> listResult = session.createQuery(queryString)
					.setParameter("stDate", lastMonth, TemporalType.TIMESTAMP)
					.setParameter("edDate", date, TemporalType.TIMESTAMP)
					.getResultList();
			if (listResult != null) {
				for (Object obj : listResult) {
					if (obj instanceof IpInfo) {
						if (((IpInfo) obj).getCreateTime() != null
								&&
								((IpInfo) obj).getCreateTime()
										.after(lastMonth)) {
							count++;
						}
					}
				}
			}
			ta.commit();
		} catch (HibernateException ex) {
			if (ta != null) {
				ta.rollback();
			}
		} finally {
			session.close();
		}
		return count;
	}

	@SuppressWarnings("unchecked")
	public static List<LocationViewBean> getTopLocation(int top) {
		List<LocationViewBean> result = new ArrayList<LocationViewBean>();
		Session session = HibernateUtil.getSession();
		Transaction ta = null;

		if (top <= 0)
			top = 10; // default is 10

		try {
			String queryString = "SELECT count(ipinfo.country), ipinfo.country FROM IpInfo as ipinfo GROUP BY ipinfo.country";

			ta = session.beginTransaction();
			List<Object[]> listResult = session.createQuery(queryString)
					.setFirstResult(0)
					.setMaxResults(top)
					.getResultList();
			for (Object[] arr : listResult) {
				LocationViewBean bean = new LocationViewBean();
				bean.setCount(((Long) arr[0]).intValue());
				bean.setCountry((String) arr[1]);
				result.add(bean);
			}
			ta.commit();
		} catch (HibernateException ex) {
			if (ta != null) {
				ta.rollback();
			}
			log.error(ex.getStackTrace());
		} catch (Exception ex) {
			log.error(ex.getStackTrace());
		} finally {
			session.close();
		}
		return result;
	}
}
