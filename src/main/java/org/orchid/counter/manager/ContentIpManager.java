package org.orchid.counter.manager;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.TemporalType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.orchid.counter.models.ContentIp;
import org.orchid.utils.HibernateUtil;

public class ContentIpManager {

	private static Log log = LogFactory.getLog(ContentIpManager.class);

	public static void create(ContentIp contentIp) {
		Session session = HibernateUtil.getSession();
		Transaction ta = null;
		try {
			ta = session.beginTransaction();
			session.saveOrUpdate(contentIp);
			ta.commit();
		} catch (HibernateException ex) {
			if (ta != null) {
				ta.rollback();
			}
			log.error(ex.getStackTrace());
		} finally {
			session.close();
		}
	}

	public static void update(ContentIp contentIp) {
		Session session = HibernateUtil.getSession();
		Transaction ta = null;
		try {
			ta = session.beginTransaction();
			session.update(contentIp);
			ta.commit();
		} catch (HibernateException ex) {
			if (ta != null) {
				ta.rollback();
			}
			log.error(ex.getStackTrace());
		} finally {
			session.close();
		}
	}

	public static int getRageViewsPerMonth(Date date) {
		int count = 0;
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, -1);
		Date lastMonth = cal.getTime();

		Session session = HibernateUtil.getSession();
		Transaction ta = null;

		try {
			ta = session.beginTransaction();

			String queryString = "FROM ContentIp AS c WHERE c.commingTime BETWEEN :stDate AND :edDate ";
			List<?> listResult = session.createQuery(queryString)
				.setParameter("stDate", lastMonth, TemporalType.TIMESTAMP)
				.setParameter("edDate", date, TemporalType.TIMESTAMP)
				.getResultList();

			if (listResult != null) {
				count = listResult.size();
			}
			ta.commit();
		} catch (HibernateException ex) {
			if (ta != null) {
				ta.rollback();
			}
			log.error(ex.getStackTrace());
		} finally {
			session.close();
		}
		return count;
	}

}
