package org.orchid.counter.manager;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.orchid.counter.bean.ContentViewBean;
import org.orchid.counter.models.Content;
import org.orchid.utils.HibernateUtil;

public class ContentManager {

	private static Log log = LogFactory.getLog(ContentManager.class);

	public static Content findByUri(String uri) {
		Content content = null;
		Session session = HibernateUtil.getSession();
		Transaction ta = null;

		try {
			String queryString = "SELECT content FROM Content as content WHERE content.uri = :uri";
			ta = session.beginTransaction();
			
			List<Content> listResult = session.createQuery(queryString, Content.class)
					.setParameter("uri", uri)
					.getResultList();
			
			if (listResult.size() > 0) {
				content = listResult.get(0);
			}
			
			ta.commit();
		} catch (HibernateException ex) {
			if (ta != null) {
				ta.rollback();
			}
			log.error(ex.getStackTrace());
		} finally {
			session.close();
		}
		return content;
	}

	public static void create(Content content) {
		Session session = HibernateUtil.getSession();
		Transaction ta = null;
		try {
			ta = session.beginTransaction();
			session.saveOrUpdate(content);
			ta.commit();
		} catch (HibernateException ex) {
			if (ta != null) {
				ta.rollback();
			}
			log.error(ex.getStackTrace());
		} finally {
			session.close();
		}
	}

	public static void update(Content content) {
		Session session = HibernateUtil.getSession();
		Transaction ta = null;
		try {
			ta = session.beginTransaction();
			session.update(content);
			ta.commit();
		} catch (HibernateException ex) {
			if (ta != null) {
				ta.rollback();
			}
			log.error(ex.getStackTrace());
		} finally {
			session.close();
		}
	}

	public static int getAllPageViews() {
		int count = 0;
		Session session = HibernateUtil.getSession();
		Transaction ta = null;

		try {
			ta = session.beginTransaction();
			
			List<?> listResult = session.createQuery("FROM Content").getResultList();
			for (Object obj : listResult) {
				if (obj instanceof Content) {
					count += ((Content) obj).getCount();
				}
			}
			ta.commit();
		} catch (HibernateException ex) {
			if (ta != null) {
				ta.rollback();
			}
			log.error(ex.getStackTrace());
		} finally {
			session.close();
		}
		return count;
	}

	public static List<ContentViewBean> getTopContent(int top) {
		List<ContentViewBean> result = new ArrayList<ContentViewBean>();
		Session session = HibernateUtil.getSession();
		Transaction ta = null;

		if (top <= 0)
			top = 10; // default is 10

		try {
			String queryString = "SELECT content FROM Content as content ORDER BY content.count DESC";

			ta = session.beginTransaction();
			List<?> listResult = session.createQuery(queryString)
				.setFirstResult(0)
				.setMaxResults(top)
				.getResultList();

			for (Object obj : listResult) {
				ContentViewBean bean = new ContentViewBean();
				bean.setCount(((Content) obj).getCount());
				bean.setUri(((Content) obj).getUri());
				result.add(bean);
			}
			ta.commit();
		} catch (HibernateException ex) {
			if (ta != null) {
				ta.rollback();
			}
			log.error(ex.getStackTrace());
		} finally {
			session.close();
		}
		return result;
	}

}
