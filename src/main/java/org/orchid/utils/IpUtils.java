package org.orchid.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.orchid.counter.models.IpInfo;

import com.google.gson.Gson;

public class IpUtils {
	public static final String _255 = "(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";
	public static final Pattern pattern = Pattern.compile("^(?:" + _255
			+ "\\.){3}" + _255 + "$");

	public static String longToIpV4(long longIp) {
		int octet3 = (int) ((longIp >> 24) % 256);
		int octet2 = (int) ((longIp >> 16) % 256);
		int octet1 = (int) ((longIp >> 8) % 256);
		int octet0 = (int) ((longIp) % 256);
		return octet3 + "." + octet2 + "." + octet1 + "." + octet0;
	}

	public static long ipV4ToLong(String ip) {
		String[] octets = ip.split("\\.");
		return (Long.parseLong(octets[0]) << 24)
				+ (Integer.parseInt(octets[1]) << 16) +
				(Integer.parseInt(octets[2]) << 8)
				+ Integer.parseInt(octets[3]);
	}

	public static boolean isIPv4Private(String ip) {
		long longIp = ipV4ToLong(ip);
		return (longIp >= ipV4ToLong("10.0.0.0") && longIp <= ipV4ToLong("10.255.255.255"))
				||
				(longIp >= ipV4ToLong("172.16.0.0") && longIp <= ipV4ToLong("172.31.255.255"))
				||
				longIp >= ipV4ToLong("192.168.0.0")
				&& longIp <= ipV4ToLong("192.168.255.255");
	}

	public static boolean isIPv4Valid(String ip) {
		return pattern.matcher(ip).matches();
	}

	public static String getIpAddr(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("X_FORWARDED_FOR");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}

	public static String getIpFromRequest(HttpServletRequest request) {
		String ip;
		boolean found = false;
		if ((ip = getIpAddr(request)) != null) {
			StringTokenizer tokenizer = new StringTokenizer(ip, ",");
			while (tokenizer.hasMoreTokens()) {
				ip = tokenizer.nextToken().trim();
				if (isIPv4Valid(ip) && !isIPv4Private(ip)) {
					found = true;
					break;
				}
			}
		}
		if (!found) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}

	public static IpInfo getIpInfoFromRequest(HttpServletRequest request) {
		return getIpInfo(getIpFromRequest(request));
	}

	public static IpInfo getIpInfo(String ip) {
		IpInfo ipInfo = null;

		if ("127.0.0.1".equals(ip)) {
			ipInfo = new IpInfo();
			ipInfo.setIp(ip);
			ipInfo.setHostname("localhost");
			ipInfo.setCountry("");
			ipInfo.setCity("");
			ipInfo.setRegion("");
			ipInfo.setLoc("");
			ipInfo.setOrg("");
			ipInfo.setPostal("");
		}
		else if (isIPv4Private(ip)) {
			ipInfo = new IpInfo();
			ipInfo.setIp(ip);
			ipInfo.setHostname("local-maejo");
			ipInfo.setCountry("TH");
			ipInfo.setCity("Sansai");
			ipInfo.setRegion("Chiang Mai");
			ipInfo.setLoc("18.8307,99.0038");
			ipInfo.setOrg("AS9931 The Communication Authoity of Thailand, CAT");
			ipInfo.setPostal("50290");
		} else {
			try {
				CloseableHttpClient httpclient = HttpClients.createDefault();
				HttpGet httpGet = new HttpGet(
						"http://ipinfo.io/" + ip + "/json");

				HttpResponse res = httpclient.execute(httpGet);
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(
								res.getEntity().getContent()));

				Gson gson = new Gson();
				ipInfo = gson.fromJson(reader, IpInfo.class);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return ipInfo;
	}
}