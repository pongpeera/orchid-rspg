package org.orchid.utils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class OrchidUtil {
    public static void goTo(String url, HttpServletRequest request, HttpServletResponse response, ServletContext servletContext) {
        if (url != null) {
            RequestDispatcher dispatcher = servletContext.getRequestDispatcher(url);
            try {
                dispatcher.forward(request, response);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
