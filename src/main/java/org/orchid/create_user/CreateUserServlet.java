package org.orchid.create_user;

import org.json.JSONObject;
import org.orchid.models.Role;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

@WebServlet(name = "CreateUserServlet")
public class CreateUserServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        boolean result = false;
        try {

            String email = request.getParameter("email");
            String password = request.getParameter("password");
            int selectedRole = Integer.parseInt(request.getParameter("selectedRole"));
            String name = request.getParameter("name");
            String telephoneNumber = request.getParameter("telephoneNumber");
            String department = request.getParameter("department");
            String personalId = request.getParameter("personalId");

            CreateUserManager manager = new CreateUserManager();
            manager.getLogin().setEmail(email);
            manager.getLogin().setPassword(password);
            manager.getLogin().getPerson().setPersonalId(personalId);
            manager.getLogin().getPerson().setName(name);
            manager.getLogin().getPerson().setDepartment(department);
            manager.getLogin().getPerson().setTelephoneNumber(telephoneNumber);
            manager.getLogin().getPerson().getRole().add(new Role(selectedRole,manager.getLogin().getPerson()));

            result = manager.createUser();

        } catch (NullPointerException e) {
            System.out.println("NullPointerException from CreateUserServlet : "+e.getMessage());
        } catch (Exception e) {
            System.out.println("Exception from CreateUserServlet : "+e.getMessage());
        }
        JSONObject json = new JSONObject();
        json.put("result",result);
        response.getWriter().append(json.toString());
    }

}
