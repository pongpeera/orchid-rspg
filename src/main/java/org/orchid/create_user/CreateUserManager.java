package org.orchid.create_user;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.orchid.models.Login;
import org.orchid.models.Role;
import org.orchid.utils.HibernateUtil;

import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

public class CreateUserManager {
    private Login login;

    public CreateUserManager () {
        this.login = new Login();
    }

    public Login getLogin() {
        return login;
    }

    public boolean createUser () {
        Session hibernateSession = HibernateUtil.getSession();
        try {
            hibernateSession.save(this.login.getPerson());
            hibernateSession.save(this.login);
            hibernateSession.beginTransaction().commit();
            for (Role r : this.login.getPerson().getRole()) {
                StoredProcedureQuery query = hibernateSession.createStoredProcedureQuery("p_addRole")
                        .registerStoredProcedureParameter(1,Integer.class, ParameterMode.IN)
                        .registerStoredProcedureParameter(2,String.class, ParameterMode.IN);
                query.setParameter(1,r.getRoleId());
                query.setParameter(2,this.login.getPerson().getPersonalId());
                query.execute();
            }
            hibernateSession.close();
            return true;
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return false;
    }
}
