package org.orchid.add_orchid;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.orchid.models.Orchid;
import org.orchid.utils.HibernateUtil;

public class AddOrchidManager {
    private Orchid orchid;


    public AddOrchidManager () {
        this.orchid = new Orchid();
    }

    public Orchid getOrchid() {
        return orchid;
    }

    public void setOrchid(Orchid orchid) {
        this.orchid = orchid;
    }

    public boolean insertOrchid () {
        Session hibernateSession = HibernateUtil.getSession();
        try {
            hibernateSession.save(this.orchid);
            hibernateSession.beginTransaction().commit();
            hibernateSession.close();
            return true;
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return false;
    }

}
