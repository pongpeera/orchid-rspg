package org.orchid.remove_log;

import org.json.JSONArray;
import org.json.JSONObject;
import org.orchid.models.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "RemoveLogServlet")
public class RemoveLogServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        RemoveLogManager manager = new RemoveLogManager();
        JSONArray json = new JSONArray();
        try {
            ArrayList<RemoveLog> logs = manager.findAllRemoveLog();
            for (RemoveLog log : logs) {
                JSONObject jsonLog = new JSONObject();
                String classType = log.getClass().getSimpleName();
                jsonLog.put("type",log.getClass().getSimpleName());
                jsonLog.put("removeDate",log.getRemoveDate());
                jsonLog.put("description",log.getDescription());

                if ("TreeRemoveLog".equalsIgnoreCase(classType)) {
                    TreeRemoveLog treeRemoveLog = (TreeRemoveLog)log;
                    int id = Integer.parseInt(treeRemoveLog.getTreeID());

                    jsonLog.put("treeId",id);
                    /*Tree tree = treeManager.getTreeById(id);
                    jsonLog.put("treeName",tree.getTreeSpecies().getTreeSpeciesName());
                    jsonLog.put("treeGarden",tree.getGarden().getLocation());*/

                } else {
                    OrchidObjectRemoveLog orchidObjectRemoveLog = (OrchidObjectRemoveLog) log;
                    jsonLog.put("orchidObjectId",orchidObjectRemoveLog.getOrchidObjectID());
                    jsonLog.put("orchidId",orchidObjectRemoveLog.getOrchidID());
                    /*OrchidObject orchid = new OrchidObject();
                    orchid.setOrchidObjectId(id);
                    orchidManger.setOrchidObject(orchid);
                    orchid = orchidManger.getOrchidObjectById();
                    jsonLog.put("orchidId",orchid.getOrchid().getOrchidId());
                    jsonLog.put("orchidLocalName",orchid.getOrchid().getLocalName());
                    jsonLog.put("orchidNameOfScience",orchid.getOrchid().getNameOfScience());*/

                }
                json.put(jsonLog);
            }
            response.getWriter().append(json.toString());
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }
}
