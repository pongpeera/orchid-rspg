package org.orchid.remove_log;


import org.hibernate.Session;
import org.hibernate.query.Query;
import org.orchid.models.OrchidObjectRemoveLog;
import org.orchid.models.RemoveLog;
import org.orchid.models.TreeRemoveLog;
import org.orchid.utils.HibernateUtil;

import java.util.ArrayList;
import java.util.List;

public class RemoveLogManager {

    public ArrayList<TreeRemoveLog> findAllTreeRemoveLog () {
        ArrayList<TreeRemoveLog> logs = new ArrayList<TreeRemoveLog>();
        String hql = "FROM TreeRemoveLog";
        Session hibernateSession = HibernateUtil.getSession();
        try {
            Query query = hibernateSession.createQuery(hql);
            List result = query.list();
            for (int i = 0 ; i < result.size() ; i++) {
                logs.add((TreeRemoveLog) result.get(i));
            }
            return logs;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public ArrayList<OrchidObjectRemoveLog> findAllOrchidObjectRemoveLog () {
        ArrayList<OrchidObjectRemoveLog> logs = new ArrayList<OrchidObjectRemoveLog>();
        String hql = "FROM OrchidObjectRemoveLog";
        Session hibernateSession = HibernateUtil.getSession();
        try {
            Query query = hibernateSession.createQuery(hql);
            List result = query.list();
            for (int i = 0 ; i < result.size() ; i++) {
                logs.add((OrchidObjectRemoveLog) result.get(i));
            }
            return logs;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public ArrayList<RemoveLog> findAllRemoveLog () {
        ArrayList<RemoveLog> logs = new ArrayList<RemoveLog>();
        String hql = "FROM RemoveLog";
        Session hibernateSession = HibernateUtil.getSession();
        try {
            Query query = hibernateSession.createQuery(hql);
            List result = query.list();
            for (int i = 0 ; i < result.size() ; i++) {
                logs.add((RemoveLog) result.get(i));
            }
            return logs;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }
}
