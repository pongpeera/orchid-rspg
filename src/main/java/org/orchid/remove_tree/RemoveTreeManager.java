package org.orchid.remove_tree;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.orchid.models.Tree;
import org.orchid.models.TreeRemoveLog;
import org.orchid.utils.HibernateUtil;

public class RemoveTreeManager {
    private Tree tree;
    private TreeRemoveLog removeLog;

    public RemoveTreeManager() {
        this.tree = new Tree();
        this.removeLog = new TreeRemoveLog();
    }

    public Tree getTree() {
        return tree;
    }

    public void setTree(Tree tree) {
        this.tree = tree;
    }

    public TreeRemoveLog getRemoveLog() {
        return removeLog;
    }

    public void setRemoveLog(TreeRemoveLog removeLog) {
        this.removeLog = removeLog;
    }

    public boolean deleteTree() {
        Session hibernateSession = HibernateUtil.getSession();
        try {
            hibernateSession.delete(this.tree);
            hibernateSession.beginTransaction().commit();
            hibernateSession.close();
            return true;
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
            hibernateSession.beginTransaction().rollback();
        }
        return false;
    }

    public boolean insertRemoveReason () {
        Session hibernateSession = HibernateUtil.getSession();
        try {
            hibernateSession.save(this.removeLog);
            hibernateSession.beginTransaction().commit();
            hibernateSession.close();
            return true;
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
            hibernateSession.beginTransaction().rollback();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return false;
    }
}
