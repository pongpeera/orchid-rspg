package org.orchid.remove_tree;

import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "RemoveTreeServlet")
public class RemoveTreeServlet extends HttpServlet {
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        JSONObject json = new JSONObject();
        boolean isDeleted = false;
        try {
            RemoveTreeManager manager = new RemoveTreeManager();
            int treeId = Integer.parseInt(request.getParameter("treeId"));
            manager.getTree().setTreeId(treeId);
            isDeleted = manager.deleteTree();
            String description = request.getParameter("description");
            String removeDate = request.getParameter("removeDate");
            if (isDeleted && request.getParameter("description") != null && request.getParameter("removeDate") != null) {


                manager.getRemoveLog().setTreeID(Integer.toString(treeId));
                manager.getRemoveLog().setRemoveDate(removeDate);
                manager.getRemoveLog().setDescription(description);
                manager.insertRemoveReason();
            }

        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
        json.put("result",isDeleted);
        response.getWriter().append(json.toString());
    }
}
