package org.orchid.add_orchid_object;


import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.orchid.models.OrchidObject;
import org.orchid.utils.HibernateUtil;

public class AddOrchidObjectManager {
    private OrchidObject orchidObject;

    public AddOrchidObjectManager () {
        this.orchidObject = new OrchidObject();
    }

    public OrchidObject getOrchidObject() {
        return orchidObject;
    }

    public void setOrchidObject(OrchidObject orchidObject) {
        this.orchidObject = orchidObject;
    }

    public boolean insertOrchidObject () {
        Session hibernateSession = HibernateUtil.getSession();
        try {
            hibernateSession.save(this.orchidObject);
            hibernateSession.beginTransaction().commit();
            hibernateSession.close();
            return true;
        } catch (HibernateException e) {
            System.out.print(e.getMessage());
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
        return false;
    }
}
