package org.orchid.add_orchid_object;

import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(name = "AddOrchidObjectServlet")
public class AddOrchidObjectServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        AddOrchidObjectManager manager = new AddOrchidObjectManager();
        JSONObject json = new JSONObject();
        boolean result = false;
        try {
            int treeId = Integer.parseInt(request.getParameter("treeId"));
            int orchidId = Integer.parseInt(request.getParameter("selectOrchidSpecies"));
            int orchidObjectId  = Integer.parseInt(request.getParameter("orchidObjectId"));

            manager.getOrchidObject().setOrchidObjectId(orchidObjectId);
            manager.getOrchidObject().getTree().setTreeId(treeId);
            manager.getOrchidObject().getOrchid().setOrchidId(orchidId);
            result = manager.insertOrchidObject();
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
        json.put("result",result);
        response.getWriter().append(json.toString());
    }
}