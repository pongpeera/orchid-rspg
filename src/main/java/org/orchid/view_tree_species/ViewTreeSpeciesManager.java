package org.orchid.view_tree_species;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.orchid.models.TreeSpecies;
import org.orchid.utils.HibernateUtil;

import java.util.ArrayList;
import java.util.List;

public class ViewTreeSpeciesManager {
    private TreeSpecies species;

    public TreeSpecies getSpecies() {
        return species;
    }

    public void setSpecies(TreeSpecies species) {
        this.species = species;
    }

    public ViewTreeSpeciesManager () {
        this.species = new TreeSpecies();
    }

    public ArrayList<TreeSpecies> findAllTreeSpecies() {
        ArrayList<TreeSpecies> treeSpecies = new ArrayList<TreeSpecies>();
        String hql = "FROM TreeSpecies";
        Session hibernateSession = HibernateUtil.getSession();
        try {
            Query query = hibernateSession.createQuery(hql);
            List result = query.list();
            for (int i = 0; i < result.size(); i++) {
                treeSpecies.add((TreeSpecies) result.get(i));
            }
            return treeSpecies;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }
}
