package org.orchid.view_tree_species;

import org.json.JSONArray;
import org.json.JSONObject;
import org.orchid.models.TreeSpecies;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "ViewTreeSpeciesServlet")
public class ViewTreeSpeciesServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        JSONArray jsonArray = new JSONArray();
        ViewTreeSpeciesManager manager = new ViewTreeSpeciesManager();
        ArrayList<TreeSpecies> treeSpecies = manager.findAllTreeSpecies();
        if (treeSpecies != null && treeSpecies.size() > 0) {
            for (TreeSpecies species : treeSpecies) {
                JSONObject json = new JSONObject();
                json.put("id",species.getTreeSpeciesId());
                json.put("name",species.getTreeSpeciesName());
                jsonArray.put(json);
            }
        }
        response.getWriter().append(jsonArray.toString());
    }
}
