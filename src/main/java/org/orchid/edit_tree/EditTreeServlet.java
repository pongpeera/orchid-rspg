package org.orchid.edit_tree;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(name = "EditTreeServlet")
public class EditTreeServlet extends HttpServlet {
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        try {
            EditTreeManager manager = new EditTreeManager();


            int treeId = Integer.parseInt(request.getParameter("treeId"));
            int speciesId = Integer.parseInt(request.getParameter("speciesId"));
            int gardenId = Integer.parseInt(request.getParameter("gardenId"));
            String latitude = request.getParameter("latitude");
            String longitude = request.getParameter("longitude");

            manager.getTree().setTreeId(treeId);
            manager.getTree().setLatitude(latitude);
            manager.getTree().setLongitude(longitude);
            manager.getTree().getGarden().setGardenId(gardenId);
            manager.getTree().getTreeSpecies().setTreeSpeciesId(speciesId);

            manager.updateTree();
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }
}
