package org.orchid.add_tree;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.orchid.models.Tree;
import org.orchid.utils.HibernateUtil;

public class AddTreeManager {
    private Tree tree;

    public AddTreeManager() {
        this.tree = new Tree();
    }

    public Tree getTree() {
        return tree;
    }

    public void setTree(Tree tree) {
        this.tree = tree;
    }

    public boolean insertTree () {
        Session hibernateSession = HibernateUtil.getSession();
        try {
            hibernateSession.save(this.tree);
            hibernateSession.beginTransaction().commit();
            hibernateSession.close();
            return true;
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
            hibernateSession.beginTransaction().rollback();
        }
        return false;
    }
}
