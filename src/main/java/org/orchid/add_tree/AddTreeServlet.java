package org.orchid.add_tree;

import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "AddTreeServlet")
public class AddTreeServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        JSONObject json = new JSONObject();
        boolean result = false;
        try {

            int speciesId = Integer.parseInt(request.getParameter("speciesId"));
            int gardenId = Integer.parseInt(request.getParameter("gardenId"));
            String latitude = request.getParameter("latitude");
            String longitude = request.getParameter("longitude");

            AddTreeManager manager = new AddTreeManager();

            manager.getTree().setLatitude(latitude);
            manager.getTree().setLongitude(longitude);
            manager.getTree().getGarden().setGardenId(gardenId);
            manager.getTree().getTreeSpecies().setTreeSpeciesId(speciesId);

            result = manager.insertTree();

        } catch (Exception e) {
            System.out.print(e.getMessage());
        }

        json.put("isSuccess",result);
        response.getWriter().append(json.toString());
    }
}
