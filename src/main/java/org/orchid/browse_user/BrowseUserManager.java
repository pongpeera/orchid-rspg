package org.orchid.browse_user;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.json.JSONArray;
import org.json.JSONObject;
import org.orchid.models.Constraints;
import org.orchid.models.Login;
import org.orchid.models.Person;
import org.orchid.models.Role;
import org.orchid.utils.HibernateUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class BrowseUserManager {
    public Login login;

    public BrowseUserManager () {
        this.login = new Login();
    }

    public Login getLogin() {
        return login;
    }


    public ArrayList<Login> findAllUser () {
        String hql = "FROM  Login l";
        ArrayList<Login> logins = new ArrayList<Login>();
        Session hibernateSession = HibernateUtil.getSession();
        try {
            Query query = hibernateSession.createQuery(hql);
            List result = query.list();
            for (int i = 0 ; i < result.size() ; i++) {
                logins.add((Login) result.get(i));
            }
            hibernateSession.close();
            return logins;
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
        return null;
    }

    public static JSONObject toJson(Login login) {
        JSONObject jsonUser = new JSONObject();
        if (login == null) return  jsonUser;
        Person person = login.getPerson();
        Set<Role> roleSet = person.getRole();
        JSONArray jsonRoleArray = new JSONArray();

        jsonUser.put("email",login.getEmail());
        jsonUser.put("name",person.getName());
        jsonUser.put("telephoneNumber",person.getTelephoneNumber());
        jsonUser.put("department",person.getDepartment());

        for (Role role : roleSet) {
            if (role.getRoleId() == 0) return jsonUser;
            JSONObject jsonRole = new JSONObject();
            JSONArray jsonConstraintArray = new JSONArray();
            jsonRole.put("id",role.getRoleId());
            jsonRole.put("name",role.getRoleName());
            for (Constraints constraint : role.getConstraint()) {
                JSONObject jsonConstraint = new JSONObject();
                jsonConstraint.put("id",constraint.getConstraintId());
                jsonConstraint.put("name",constraint.getConstraintName());
                jsonConstraintArray.put(jsonConstraint);
            }
            jsonRole.put("constraints",jsonConstraintArray);
            jsonRoleArray.put(jsonRole);
        }
        jsonUser.put("roles",jsonRoleArray);
        return jsonUser;
    }
}
