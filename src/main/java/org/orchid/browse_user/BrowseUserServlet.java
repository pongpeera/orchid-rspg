package org.orchid.browse_user;

import org.json.JSONArray;
import org.json.JSONObject;
import org.orchid.models.Login;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "BrowseUserServlet")
public class BrowseUserServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        BrowseUserManager manager = new BrowseUserManager();
        ArrayList<Login> loginList = manager.findAllUser();
        JSONArray json = new JSONArray();
        for (Login login : loginList) {
            JSONObject jsonLogin = BrowseUserManager.toJson(login);
            json.put(jsonLogin);
        }
        response.getWriter().append(json.toString());
    }
}
