package org.orchid.remove_user;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.orchid.models.Login;
import org.orchid.utils.HibernateUtil;

import java.util.List;

public class RemoveUserManager {
    public Login login;

    public RemoveUserManager () {
        this.login = new Login();
    }


    public Login getLogin() {
        return login;
    }

    public void setLogin(Login login) {
        this.login = login;
    }

    public Login findUserByEmail () {
        String hql = "FROM  Login  WHERE email = :email";
        Session hibernateSession = HibernateUtil.getSession();
        try {
            Query query = hibernateSession.createQuery(hql);
            query.setParameter("email",this.login.getEmail());
            List result = query.list();
            hibernateSession.close();
            return (Login) result.get(0);
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
        return null;
    }



    public boolean deleteUser () {
        Login login = findUserByEmail();
        Session hibernateSession = HibernateUtil.getSession();
        try {
            hibernateSession.delete(login);
            hibernateSession.beginTransaction().commit();
            hibernateSession.close();
            return true;
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
        return  false;
    }
}
