package org.orchid.remove_user;

import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "RemoveUserServlet")
public class RemoveUserServlet extends HttpServlet {
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        String email = request.getParameter("email");
        RemoveUserManager manager = new RemoveUserManager();
        manager.getLogin().setEmail(email);
        JSONObject json = new JSONObject();
        boolean result =  manager.deleteUser();
        json.put("result",result);
        response.getWriter().append(json.toString());
    }
}
