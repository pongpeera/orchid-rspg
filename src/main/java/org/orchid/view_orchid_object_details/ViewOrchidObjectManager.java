package org.orchid.view_orchid_object_details;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.json.JSONObject;
import org.orchid.models.OrchidObject;
import org.orchid.utils.HibernateUtil;

import java.util.List;

public class ViewOrchidObjectManager {
    private OrchidObject orchidObject;

    public ViewOrchidObjectManager() {
        this.orchidObject = new OrchidObject();
    }

    public OrchidObject getOrchidObject() {
        return orchidObject;
    }

    public void setOrchidObject(OrchidObject orchidObject) {
        this.orchidObject = orchidObject;
    }
    public OrchidObject findOrchidObjectById () {
        String hql = "FROM  OrchidObject  WHERE orchidObjectId = :orchidObjectId";
        Session hibernateSession = HibernateUtil.getSession();
        try {
            Query query = hibernateSession.createQuery(hql);
            query.setParameter("orchidObjectId",this.orchidObject.getOrchidObjectId());
            List result = query.list();
            hibernateSession.close();
            return (OrchidObject)result.get(0);
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
        return null;
    }


    public static JSONObject toJson(OrchidObject orchidObject) {
        JSONObject jsonObject = new JSONObject();
        JSONObject jsonTree = new JSONObject();
        JSONObject jsonOrchid = new JSONObject();
        JSONObject jsonGarden = new JSONObject();

        jsonGarden.put("id",orchidObject.getTree().getGarden().getGardenId());
        jsonGarden.put("name",orchidObject.getTree().getGarden().getLocation());
        jsonTree.put("garden",jsonGarden);

        jsonTree.put("id",orchidObject.getTree().getTreeId());
        jsonTree.put("id",orchidObject.getTree().getTreeId());
        jsonTree.put("name",orchidObject.getTree().getTreeSpecies().getTreeSpeciesName());

        jsonOrchid.put("id",orchidObject.getOrchid().getOrchidId());
        jsonOrchid.put("nameOfScience",orchidObject.getOrchid().getNameOfScience());
        jsonOrchid.put("localName",orchidObject.getOrchid().getLocalName());
        jsonOrchid.put("flower",orchidObject.getOrchid().getMonthOfFlower());
        jsonOrchid.put("endFlower",orchidObject.getOrchid().getMonthOfEndFlower());

        jsonObject.put("id",orchidObject.getOrchidObjectId());
        jsonObject.put("tree",jsonTree);
        jsonObject.put("orchid",jsonOrchid);
        return jsonObject;
    }
}
