package org.orchid.view_orchid_object_details;

import org.json.JSONArray;
import org.json.JSONObject;
import org.orchid.models.OrchidObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "ViewOrchidObjectServlet")
public class ViewOrchidObjectServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        ViewOrchidObjectManager manager = new ViewOrchidObjectManager();
        int id = Integer.parseInt(request.getParameter("orchidObjectId"));
        manager.getOrchidObject().setOrchidObjectId(id);
        OrchidObject orchidObject = manager.findOrchidObjectById();
        JSONObject json = ViewOrchidObjectManager.toJson(orchidObject);
        response.getWriter().append(json.toString());
    }
}
