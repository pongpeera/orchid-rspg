package org.orchid.edit_orchid;

import org.json.JSONObject;
import org.orchid.models.Benefit;
import org.orchid.models.OrchidStatus;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "EditOrchidServlet")
public class EditOrchidServlet extends HttpServlet {
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        EditOrchidManager manager = new EditOrchidManager();
        JSONObject json = new JSONObject();
        boolean updated = false;
        try {
            int orchidId = Integer.parseInt(request.getParameter("orchidId"));
            String nameOfScience = request.getParameter("nameOfScience");
            String localName = request.getParameter("localName");
            String flower = request.getParameter("flower");
            String endFlower = request.getParameter("endFlower");
            String[] statusList = request.getParameterValues("status");
            String[] benefitList = request.getParameterValues("benefit");

            for (String id : statusList) {
                manager.getOrchid().getOrchidStatus().add(new OrchidStatus(Integer.parseInt(id)));
            }

            for (String id : benefitList) {
                manager.getOrchid().getBenefits().add(new Benefit(Integer.parseInt(id)));
            }

            manager.getOrchid().setOrchidId(orchidId);
            manager.getOrchid().setNameOfScience(nameOfScience);
            manager.getOrchid().setLocalName(localName);
            manager.getOrchid().setMonthOfFlower(flower);
            manager.getOrchid().setMonthOfEndFlower(endFlower);

            updated = manager.updateOrchid();

        } catch (NullPointerException e) {
            System.out.print(e.getMessage());
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
        json.put("completed",updated);
        response.getWriter().append(json.toString());
    }
}
