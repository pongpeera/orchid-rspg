package org.orchid.edit_orchid;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.orchid.models.Orchid;
import org.orchid.utils.HibernateUtil;

public class EditOrchidManager {
    private Orchid orchid;


    public EditOrchidManager() {
        this.orchid = new Orchid();
    }

    public Orchid getOrchid() {
        return orchid;
    }

    public void setOrchid(Orchid orchid) {
        this.orchid = orchid;
    }
    public boolean updateOrchid () {
        Session hibernateSession = HibernateUtil.getSession();
        try {
            hibernateSession.update(this.orchid);
            hibernateSession.beginTransaction().commit();
            hibernateSession.close();
            return true;
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return false;
    }
}
