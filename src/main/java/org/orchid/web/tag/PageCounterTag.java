package org.orchid.web.tag;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.orchid.counter.manager.ContentIpManager;
import org.orchid.counter.manager.ContentManager;
import org.orchid.counter.manager.IpInfoManager;
import org.orchid.counter.models.Content;
import org.orchid.counter.models.ContentIp;
import org.orchid.counter.models.IpInfo;
import org.orchid.utils.IpUtils;

import com.google.common.base.Strings;

public class PageCounterTag extends SimpleTagSupport {

	private static final int DEFAULT_DEGIT = 4;

	private Log log = LogFactory.getLog(PageCounterTag.class);

	private String digit;
	private String username;

	private int numOfDigit;
	private int counter;

	public void doTag() throws JspException, IOException {

		log.trace("Enter counter tag...");

		BufferedReader br = null;
		StringBuffer buff = new StringBuffer();
		JspWriter out = getJspContext().getOut();

		try {
			numOfDigit = Integer.parseInt(getDigit());
		} catch (Exception e) {
			numOfDigit = DEFAULT_DEGIT;
			setDigit(String.valueOf(numOfDigit));
		}

		if (numOfDigit <= 0) {
			numOfDigit = DEFAULT_DEGIT;
		}

		try {
			String sText;

			InputStream fis = getClass()
					.getResourceAsStream("digit.txt");

			br = new BufferedReader(new InputStreamReader(fis));
			while ((sText = br.readLine()) != null) {
				buff.append(sText + "\n");
			}
			sText = buff.toString();

			int mul = 1;
			for (int i = 0; i < numOfDigit; i++) {
				mul *= 10;
			}

			counter = getCounter() % mul;
			String sVal = String.format("%0" + numOfDigit + "d", counter);
			char[] chs = sVal.toCharArray();

			out.println("<div class=\"counter-container\">");
			for (char ch : chs) {
				String newText = sText.replace("__value__", "" + ch);
				out.print(newText);
			}
			out.println("</div>");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}

			log.trace("End counter tag...");
		}
	}

	@SuppressWarnings("unchecked")
	private int getCounter() {
		int count = 0;
		HttpSession session = ((PageContext) getJspContext()).getSession();
		HttpServletRequest request = (HttpServletRequest) ((PageContext) getJspContext()).getRequest();

		HashMap<String, Object> uriMap = (HashMap<String, Object>) session.getAttribute("uriMap");

		String sPage = "";
		String sid = request.getParameter("id");
		if (!Strings.isNullOrEmpty(sid)) {
			sPage = "-" + sid;
		}
//		else {
//			Blog blog = (Blog) session.getAttribute("lastBlog");
//			if (blog != null) {
//				sPage = "-" + blog.getId();
//			}
//		}
		
		String uri = request.getRequestURI() + sPage;

		if (uriMap == null) {
			uriMap = new HashMap<String, Object>();
			session.setAttribute("uriMap", uriMap);
		}

		if (!uriMap.containsKey(uri)) {
			uriMap.put(uri, uriMap);
			count = increaseCounter(uri, request);
		} else {
			count = getCurrentCount(uri);
		}

		return count;
	}

	private int getCurrentCount(String uri) {
		Content content = ContentManager.findByUri(uri);
		if (content != null) {
			return content.getCount();
		}
		return 0;
	}

	private int increaseCounter(String uri, HttpServletRequest request) {
		IpInfo ipInfo = IpUtils.getIpInfoFromRequest(request);
		IpInfo existIpInfo = IpInfoManager.findByIp(ipInfo.getIp());
		Date curTime = new Date();

		if (existIpInfo == null) {
			ipInfo.setCreateTime(curTime);
			IpInfoManager.create(ipInfo);
		} else {
			ipInfo = existIpInfo;
		}

		if (ipInfo.getId() > 0) {
			ipInfo.setLastAccessTime(curTime);
			IpInfoManager.update(ipInfo);

			Content content = ContentManager.findByUri(uri);
			if (content == null) {
				content = new Content();
				content.setCount(1);
				content.setUri(uri);
				ContentManager.create(content);
			} else {
				content.setCount(content.getCount() + 1);
				ContentManager.update(content);
			}

			if (content.getId() > 0) {
				String userAgent = request.getHeader("User-Agent");
				ContentIp ci = new ContentIp(content, ipInfo);
				ci.setUserAgent(userAgent);
				ci.setCommingTime(curTime);
				ContentIpManager.create(ci);
				return content.getCount();
			}
		}

		return 0;
	}

	public String getDigit() {
		return digit;
	}

	public void setDigit(String digit) {
		this.digit = digit;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
