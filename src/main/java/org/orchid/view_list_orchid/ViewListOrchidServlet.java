package org.orchid.view_list_orchid;

import org.json.JSONArray;
import org.json.JSONObject;
import org.orchid.models.Orchid;
import org.orchid.view_orchid_details.ViewOrchidDetailsManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "ViewListOrchidServlet")
public class ViewListOrchidServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        ViewListOrchidManager manager = new ViewListOrchidManager();
        try {
            ArrayList<Orchid> orchids = manager.findAllOrchid();
            JSONArray json = new JSONArray();
            for (Orchid orchid : orchids) {
                JSONObject jsonObject = ViewOrchidDetailsManager.toJson(orchid);
                json.put(jsonObject);
            }
            response.getWriter().append(json.toString());
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }
}
