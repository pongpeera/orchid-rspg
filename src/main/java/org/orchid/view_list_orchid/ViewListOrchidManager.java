package org.orchid.view_list_orchid;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.json.JSONArray;
import org.json.JSONObject;
import org.orchid.models.Benefit;
import org.orchid.models.Orchid;
import org.orchid.models.OrchidStatus;
import org.orchid.utils.HibernateUtil;

import java.util.ArrayList;
import java.util.List;

public class ViewListOrchidManager {
    private Orchid orchid;

    public ViewListOrchidManager () {
        this.orchid = new Orchid();
    }

    public Orchid getOrchid() {
        return orchid;
    }

    public void setOrchid(Orchid orchid) {
        this.orchid = orchid;
    }


    public ArrayList<Orchid> findAllOrchid () {
        ArrayList<Orchid> arrOrchid = new ArrayList<Orchid>();
        String hql = "FROM Orchid";
        Session hibernateSession = HibernateUtil.getSession();
        try {
            Query query = hibernateSession.createQuery(hql);
            List result = query.list();
            hibernateSession.close();
            for (int i = 0 ; i < result.size() ; i++) {
                arrOrchid.add((Orchid) result.get(i));
            }
            hibernateSession.close();
            return arrOrchid;
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
            hibernateSession.beginTransaction().rollback();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

}
