package org.orchid.view_garden;

import org.json.JSONArray;
import org.json.JSONObject;
import org.orchid.models.Garden;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "ViewGardenServlet")
public class ViewGardenServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        JSONArray jsonArray = new JSONArray();
        ViewGardenManager manager = new ViewGardenManager();
        ArrayList<Garden> gardens = manager.findAllGarden();
        if (gardens != null && gardens.size() > 0) {
            for (Garden garden : gardens) {
                JSONObject json = new JSONObject();
                json.put("id",garden.getGardenId());
                json.put("name",garden.getLocation());
                jsonArray.put(json);
            }
        }

        response.getWriter().append(jsonArray.toString());
    }
}
