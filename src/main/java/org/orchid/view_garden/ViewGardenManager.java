package org.orchid.view_garden;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.orchid.models.Garden;
import org.orchid.utils.HibernateUtil;

import java.util.ArrayList;
import java.util.List;

public class ViewGardenManager {
    private Garden garden;

    public ViewGardenManager () {
        this.garden = new Garden();
    }

    public Garden getGarden() {
        return garden;
    }

    public void setGarden(Garden garden) {
        this.garden = garden;
    }

    public ArrayList<Garden> findAllGarden() {
        ArrayList<Garden> gardens = new ArrayList<Garden>();
        String hql = "FROM Garden";
        Session hibernateSession = HibernateUtil.getSession();
        try {
            Query query = hibernateSession.createQuery(hql);
            List result = query.list();
            for (int i = 0; i < result.size(); i++) {
                gardens.add((Garden) result.get(i));
            }
            return gardens;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }
}
