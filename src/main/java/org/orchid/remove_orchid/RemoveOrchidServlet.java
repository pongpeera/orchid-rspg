package org.orchid.remove_orchid;

import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "RemoveOrchidServlet")
public class RemoveOrchidServlet extends HttpServlet {
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        RemoveOrchidManager manager = new RemoveOrchidManager();
        JSONObject json = new JSONObject();
        boolean deleted = false;
        try {
            int orchidId = Integer.parseInt(request.getParameter("orchidId"));
            manager.getOrchid().setOrchidId(orchidId);
            deleted = manager.deleteOrchid();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        json.put("completed",deleted);
        response.getWriter().append(json.toString());
    }
}
