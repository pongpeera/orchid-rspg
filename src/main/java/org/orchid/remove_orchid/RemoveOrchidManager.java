package org.orchid.remove_orchid;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.orchid.models.Orchid;
import org.orchid.utils.HibernateUtil;

public class RemoveOrchidManager {
    private Orchid orchid;

    public RemoveOrchidManager() {
        this.orchid = new Orchid();
    }

    public Orchid getOrchid() {
        return orchid;
    }

    public void setOrchid(Orchid orchid) {
        this.orchid = orchid;
    }

    public boolean deleteOrchid () {
        Session hibernateSession = HibernateUtil.getSession();
        try {
            hibernateSession.delete(this.orchid);
            hibernateSession.beginTransaction().commit();
            hibernateSession.close();
            return true;
        } catch (HibernateException e) {
            System.out.print(e.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return false ;
    }
}
