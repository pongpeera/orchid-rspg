package org.orchid.models;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="Person")
public class Person  {
    @Id
    @Column(name="personalId")
    private String personalId;
    @Column(name="name")
    private String name;
    @Column(name="department")
    private String department;
    @Column(name="telephoneNumber")
    private String telephoneNumber;
    @OneToMany(fetch = FetchType.EAGER,mappedBy = "person")
    private Set<Role> role = new HashSet<Role>();

    public Person() {
    }

    public Person(String personalId) {
        this.personalId = personalId;
    }

    public String getPersonalId() {
        return personalId;
    }

    public void setPersonalId(String personalId) {
        this.personalId = personalId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

   /* public Set<UserConstraints> getUserConstraints() {
        return userConstraints;
    }

    public void setUserConstraints(Set<UserConstraints> userConstraints) {
        this.userConstraints = userConstraints;
    }*/
/*
    public void addRole(Role role) {
        this.role.add(role);
    }*/
   public Set<Role> getRole() {
       return role;
   }

    public void setRole(Set<Role> role) {
        this.role = role;
    }


}
