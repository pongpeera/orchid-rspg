package org.orchid.models;

import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by The Exiled on 28/7/2559.
 */
@Entity
@Table(name = "Constraints")
public class Constraints implements Serializable{
    @Id @GeneratedValue @Column(name="constraintId")
    private int constraintId;
    @Column(name="constraintName")
    private String constraintName;

    @ManyToOne(cascade = CascadeType.REMOVE,fetch = FetchType.LAZY)
    @JoinColumns({@JoinColumn(name="roleId"),@JoinColumn(name="personalId")})
    @BatchSize(size = 1)
    private Role role;

    public Constraints() {
    }

    public Constraints(int id) {
        this.constraintId = id;
    }

    public Constraints(int id , String constraintName) {
        this.constraintId = id;
        this.constraintName = constraintName;
    }

    public int getConstraintId() {
        return constraintId;
    }

    public void setConstraintId(int constraintId) {
        this.constraintId = constraintId;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }


    public String getConstraintName() {
        return constraintName;
    }

    public void setConstraintName(String constraintName) {
        this.constraintName = constraintName;
    }
}
