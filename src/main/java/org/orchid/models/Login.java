package org.orchid.models;



import javax.persistence.*;

@Entity
@Table(name="Login")
public class Login {
    @Id @Column(name="email")
    private String email;
    @Column(name="password")
    private String password;
    @OneToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER,orphanRemoval = true) @JoinColumn(name="personalId")
    private Person person;

    public Login() {
        this.person = new Person();
    }


    public Login(String email) {
        this.email = email;
        this.person = new Person();
    }


    public Login(String email, String password) {
        this.email = email;
        this.password = password;
        this.person = new Person();
    }

    public Login(String email, Person person) {
        this.email = email;
        this.person = person;
    }

    public Login(String email, String password, Person person) {
        this.email = email;
        this.password = password;
        this.person = person;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}