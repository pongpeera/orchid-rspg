package org.orchid.models;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="Orchid")
public class Orchid {

    @Id @Column(name="orchidId")
    private int orchidId;
    @Column(name="nameOfScience")
    private String nameOfScience;
    @Column(name="localName")
    private String  localName;
    @Column(name="monthOfFlower")
    private String monthOfFlower;
    @Column(name="monthOfEndFlower")
    private String  monthOfEndFlower;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "orchid_benefit", joinColumns = {
            @JoinColumn(name = "orchidId") },
            inverseJoinColumns = { @JoinColumn(name = "benefitId", nullable = false) })
    private Set<Benefit> benefits = new HashSet<Benefit>();
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "orchid_orchidStatus", joinColumns = {
            @JoinColumn(name = "orchidId") },
            inverseJoinColumns = { @JoinColumn(name = "orchidStatusId", nullable = false) })
    private Set<OrchidStatus> orchidStatus = new HashSet<OrchidStatus>();

    public Orchid() {
    }

    public Orchid(String nameOfScience, String localName, String monthOfFlower, String monthOfEndFlower, Set<Benefit> benefits, Set<OrchidStatus> orchidStatus) {
        this.nameOfScience = nameOfScience;
        this.localName = localName;
        this.monthOfFlower = monthOfFlower;
        this.monthOfEndFlower = monthOfEndFlower;
        this.benefits = benefits;
        this.orchidStatus = orchidStatus;
    }

    public int getOrchidId() {
        return orchidId;
    }

    public void setOrchidId(int orchidId) {
        this.orchidId = orchidId;
    }
    public String getNameOfScience() {
        return nameOfScience;
    }

    public void setNameOfScience(String nameOfScience) {
        this.nameOfScience = nameOfScience;
    }

    public String getLocalName() {
        return localName;
    }

    public void setLocalName(String localName) {
        this.localName = localName;
    }

    public String getMonthOfFlower() {
        return monthOfFlower;
    }

    public void setMonthOfFlower(String monthOfFlower) {
        this.monthOfFlower = monthOfFlower;
    }

    public String getMonthOfEndFlower() {
        return monthOfEndFlower;
    }

    public void setMonthOfEndFlower(String monthOfEndFlower) {
        this.monthOfEndFlower = monthOfEndFlower;
    }

    public Set<Benefit> getBenefits() {
        return benefits;
    }

    public void setBenefits(Set<Benefit> benefits) {
        this.benefits = benefits;
    }

    public Set<OrchidStatus> getOrchidStatus() {
        return orchidStatus;
    }

    public void setOrchidStatus(Set<OrchidStatus> orchidStatus) {
        this.orchidStatus = orchidStatus;
    }
}
