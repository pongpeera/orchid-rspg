package org.orchid.models;

import javax.persistence.*;

/**
 * Created by The Exiled on 28/7/2559.
 */
@Entity
@Table(name="OrchidObjectRemoveLog")
@PrimaryKeyJoinColumn(name="removeDate")
public class OrchidObjectRemoveLog extends RemoveLog {
    @Column(name="orchidObjectID")
    private String orchidObjectID;
    @Column(name="orchidID")
    private String orchidID;

    public OrchidObjectRemoveLog() {
    }

    public OrchidObjectRemoveLog(String orchidObjectID, String orchidID) {
        super();
        this.orchidObjectID = orchidObjectID;
        this.orchidID = orchidID;
    }

    public String getOrchidObjectID() {
        return orchidObjectID;
    }

    public void setOrchidObjectID(String orchidObjectID) {
        this.orchidObjectID = orchidObjectID;
    }

    public String getOrchidID() {
        return orchidID;
    }

    public void setOrchidID(String orchidID) {
        this.orchidID = orchidID;
    }
}
