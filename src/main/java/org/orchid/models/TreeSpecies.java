package org.orchid.models;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="TreeSpecies")
public class TreeSpecies {
    @Id
    @Column(name="treeSpeciesId")
    private int treeSpeciesId;
    @Column(name="treeSpeciesName")
    private String treeSpeciesName;
    @OneToMany(fetch = FetchType.EAGER,mappedBy = "treeSpecies")
    private Set<Tree> tree = new HashSet<Tree>();

    public TreeSpecies() {
    }

    public TreeSpecies(int id) {
        this.treeSpeciesId = id;
    }

    public TreeSpecies(String treeSpeciesName, Set<Tree> tree) {
        this.treeSpeciesName = treeSpeciesName;
        this.tree = tree;
    }

    public int getTreeSpeciesId() {
        return treeSpeciesId;
    }

    public void setTreeSpeciesId(int treeSpeciesId) {
        this.treeSpeciesId = treeSpeciesId;
    }

    public Set<Tree> getTree() {
        return tree;
    }

    public void setTree(Set<Tree> tree) {
        this.tree = tree;
    }

    public String getTreeSpeciesName() {
        return treeSpeciesName;
    }

    public void setTreeSpeciesName(String treeSpeciesName) {
        this.treeSpeciesName = treeSpeciesName;
    }
}
