package org.orchid.models;

import javax.persistence.*;

/**
 * Created by The Exiled on 28/7/2559.
 */
@Entity
@Table(name="Member")
@PrimaryKeyJoinColumn(name="id")
public class Member extends Role{
    @Column(name="gender")
    private String gender;
    @Column(name="address")
    private String address;
    @Column(name="birthdate")
    private String birthdate;
    @Column(name="postId")
    private String postId;

    public Member() {
    }

    public Member(String gender, String address, String birthdate, String postId,String pid) {
        super();
        this.gender = gender;
        this.address = address;
        this.birthdate = birthdate;
        this.postId = postId;
        this.setRoleId(0);
        this.setRoleName("สมาชิก");
        this.setPerson(new Person(pid));
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }
}
