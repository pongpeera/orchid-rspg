package org.orchid.models;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by The Exiled on 28/7/2559.
 */
@Entity
@Table(name="Role")
public class Role implements  Serializable {

    @Id @Column(name="roleId")
    private int roleId;
    @Column(name="roleName")
    private String roleName;
    @OneToMany(fetch = FetchType.EAGER,mappedBy = "role")
    private Set<Constraints> constraint = new HashSet<Constraints>();
    @ManyToOne(cascade = CascadeType.REMOVE,fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn(referencedColumnName = "personalId")
    @Id @BatchSize(size = 1)
    private Person person;

    public Role() {
    }

    public Role(int roleId) {
        this.roleId = roleId;
    }

    public Role(int roleId,Person person) {
        this.roleId = roleId;
        this.person = person;
    }

    public Role(int roleId, String roleName) {
        this.roleId = roleId;
        this.roleName = roleName;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Set<Constraints> getConstraint() {
        return constraint;
    }

    public void setConstraint(Set<Constraints> constraint) {
        this.constraint = constraint;
    }
}
