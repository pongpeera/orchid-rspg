package org.orchid.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="RemoveLog")
public class RemoveLog {
    @Id @Column(name="removeDate")
    private String removeDate;
    @Column(name="description")
    private String description;

    public RemoveLog() {
    }

    public RemoveLog(String removeDate, String description) {
        this.removeDate = removeDate;
        this.description = description;
    }

    public String getRemoveDate() {
        return removeDate;
    }

    public void setRemoveDate(String removeDate) {
        this.removeDate = removeDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
