package org.orchid.models;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 * Created by The Exiled on 28/7/2559.
 */
@Entity
@Table(name="Staff")
@PrimaryKeyJoinColumn(name="id")
public class Staff extends Role{

    public Staff() {
        super();
    }

}
