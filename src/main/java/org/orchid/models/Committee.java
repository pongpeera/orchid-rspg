package org.orchid.models;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 * Created by The Exiled on 28/7/2559.
 */
@Entity
@Table(name="Committee")
@PrimaryKeyJoinColumn(name="id")
public class Committee extends Role{
    public Committee() {
        super();
    }
}
