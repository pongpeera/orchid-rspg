package org.orchid.models;

import javax.persistence.*;

/**
 * Created by The Exiled on 28/7/2559.
 */
@Entity
@Table(name="TreeRemoveLog")
@PrimaryKeyJoinColumn(name="removeDate")
public class TreeRemoveLog extends RemoveLog {
    @Column(name="treeID")
    private String treeID;

    public TreeRemoveLog() {
    }

    public TreeRemoveLog(String treeID) {
        super();
        this.treeID = treeID;
    }

    public String getTreeID() {
        return treeID;
    }

    public void setTreeID(String treeID) {
        this.treeID = treeID;
    }
}
