package org.orchid.models;


import javax.persistence.*;

@Entity
@Table(name="OrchidObject")
public class OrchidObject {
    @Id @Column(name="orchidObjectId")
    private int orchidObjectId;
    @ManyToOne @JoinColumn(name="orchidId")
    private Orchid orchid;
    @ManyToOne @JoinColumn(name="treeId")
    private Tree tree;

    public OrchidObject() {
        this.orchid = new Orchid();
        this.tree = new Tree();
    }

    public OrchidObject(int orchidObjectId,Orchid orchid) {
        this.orchidObjectId = orchidObjectId;
        this.orchid = orchid;
    }

    public int getOrchidObjectId() {
        return orchidObjectId;
    }

    public void setOrchidObjectId(int orchidObjectId) {
        this.orchidObjectId = orchidObjectId;
    }
    public Orchid getOrchid() {
        return orchid;
    }

    public void setOrchid(Orchid orchid) {
        this.orchid = orchid;
    }

    public Tree getTree() {
        return tree;
    }

    public void setTree(Tree tree) {
        this.tree = tree;
    }
}
