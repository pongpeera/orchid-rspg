package org.orchid.models;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="Benefit")
public class Benefit {
    @Id @Column(name="benefitId")
    private int benefitId;
    @Column(name="benefitName")
    private String benefitName;
    @ManyToMany
    @JoinTable(name = "orchid_benefit", joinColumns = {
            @JoinColumn(name = "benefitId") },
            inverseJoinColumns = { @JoinColumn(name = "orchidId", nullable = false) })
    private Set<Orchid> orchid = new HashSet<Orchid>();

    public Benefit() {
    }

    public Benefit(int id) {
        this.benefitId = id;
    }

    public Benefit(int benefitId, String benefitName) {
        this.benefitId = benefitId;
        this.benefitName = benefitName;
    }

    public int getBenefitId() {
        return benefitId;
    }

    public void setBenefitId(int benefitId) {
        this.benefitId = benefitId;
    }

    public String getBenefitName() {
        return benefitName;
    }

    public void setBenefitName(String benefitName) {
        this.benefitName = benefitName;
    }

    public Set<Orchid> getOrchid() {
        return orchid;
    }

    public void setOrchid(Set<Orchid> orchid) {
        this.orchid = orchid;
    }
}
