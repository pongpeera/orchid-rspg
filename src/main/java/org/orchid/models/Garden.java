package org.orchid.models;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


@Entity
@Table(name="Garden")
public class Garden {
    @Id @Column(name="gardenId")
    private int gardenId;
    @Column(name="location")
    private String location;
    @OneToMany(fetch = FetchType.EAGER,mappedBy="garden")
    private Set<Tree> tree = new HashSet<Tree>();


    public Garden() {
    }

    public Garden(int id) {
        this.gardenId = id;
    }

    public Garden(int gardenId,String location,Set<Tree> tree) {
        this.gardenId = gardenId;
        this.location = location;
        this.tree = tree;
    }

    public int getGardenId() {
        return gardenId;
    }

    public void setGardenId(int gardenId) {
        this.gardenId = gardenId;
    }
    public Set<Tree> getTree() {
        return tree;
    }

    public void setTree(Set<Tree> tree) {
        this.tree = tree;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
