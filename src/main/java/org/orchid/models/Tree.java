package org.orchid.models;

import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


@Entity
@Table(name="Tree")
public class Tree {
    @Id
    @SequenceGenerator(name = "treeid_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "treeid_seq")
    @Column(name="treeId")
    private int treeId;
    @Column(name="latitude")
    private String latitude;
    @Column(name="longitude")
    private String longitude;
    @OneToMany(fetch = FetchType.EAGER,mappedBy = "tree")
    private Set<OrchidObject> orchidObject = new HashSet<OrchidObject>();
    @ManyToOne @JoinColumn(name="gardenId") @BatchSize(size = 1)
    private Garden garden;
    @ManyToOne @JoinColumn(name="treeSpeciesId") @BatchSize(size = 1)
    private TreeSpecies treeSpecies;


    public Tree() {
        this.garden = new Garden();
        this.treeSpecies = new TreeSpecies();
    }


    public Tree(int treeId,Set<OrchidObject> orchidObject) {
        this.treeId = treeId;
        this.orchidObject = orchidObject;
    }
    public int getTreeId() {
        return treeId;
    }

    public void setTreeId(int treeId) {
        this.treeId = treeId;
    }
    public Set<OrchidObject> getOrchidObject() {
        return orchidObject;
    }

    public void setOrchidObject(Set<OrchidObject> orchidObject) {
        this.orchidObject = orchidObject;
    }

    public Garden getGarden() {
        return garden;
    }

    public void setGarden(Garden garden) {
        this.garden = garden;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }


    public TreeSpecies getTreeSpecies() {
        return treeSpecies;
    }

    public void setTreeSpecies(TreeSpecies treeSpecies) {
        this.treeSpecies = treeSpecies;
    }
}
