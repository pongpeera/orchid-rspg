package org.orchid.models;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


@Entity
@Table(name = "OrchidStatus")
public class OrchidStatus {
    @Id @Column(name="orchidStatusId")
    private int orchidStatusId;
    @Column(name="orchidStatusName")
    private String orchidStatusName;
    @ManyToMany
    @JoinTable(name = "orchid_orchidStatus", joinColumns = {
            @JoinColumn(name = "orchidStatusId") },
            inverseJoinColumns = { @JoinColumn(name = "orchidId", nullable = false) })
    private Set<Orchid> orchid = new HashSet<Orchid>();

    public OrchidStatus() {
    }

    public OrchidStatus(int id) {
        this.orchidStatusId = id;
    }

    public OrchidStatus(int orchidStatusId, String orchidStatusName) {
        this.orchidStatusId = orchidStatusId;
        this.orchidStatusName = orchidStatusName;
    }

    public int getOrchidStatusId() {
        return orchidStatusId;
    }

    public void setOrchidStatusId(int orchidStatusId) {
        this.orchidStatusId = orchidStatusId;
    }

    public String getOrchidStatusName() {
        return orchidStatusName;
    }

    public void setOrchidStatusName(String orchidStatusName) {
        this.orchidStatusName = orchidStatusName;
    }

    public Set<Orchid> getOrchid() {
        return orchid;
    }

    public void setOrchid(Set<Orchid> orchid) {
        this.orchid = orchid;
    }
}
