package org.orchid.list_role;

import org.json.JSONArray;
import org.json.JSONObject;
import org.orchid.models.Constraints;
import org.orchid.models.Role;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "RoleServlet")
public class ListRoleServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        ListRoleManager manager = new ListRoleManager();
        JSONArray jsonRoles = new JSONArray();
        try {
            ArrayList<Role> roles = manager.findAllRole();
            for (Role r : roles) {
                JSONObject json = new JSONObject();
                JSONArray jsonConstraintArray = new JSONArray();
                json.put("id",r.getRoleId());
                json.put("name",r.getRoleName());
                json.put("wasRole",false);

                for (Constraints cons : r.getConstraint()) {
                    JSONObject jsonConst = new JSONObject();
                    jsonConst.put("id",cons.getConstraintId());
                    jsonConst.put("name",cons.getConstraintName());
                    jsonConst.put("haveConstraint",false);
                    jsonConstraintArray.put(jsonConst);
                }
                json.put("constraints",jsonConstraintArray);
                jsonRoles.put(json);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        response.getWriter().append(jsonRoles.toString());
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
