package org.orchid.list_role;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.orchid.models.Constraints;
import org.orchid.models.Role;
import org.orchid.utils.HibernateUtil;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class ListRoleManager {
    private Role role;

    public ListRoleManager() {
    }

    public ListRoleManager(Role role) {
        this.role = role;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public ArrayList<Role> findAllRole () {
        Session hibernateSession = HibernateUtil.getSession();
        ArrayList<Role> roles = new ArrayList<Role>();
        try {
            Query query = hibernateSession.createNativeQuery("call p_getAllRole");
            List result = query.list();
            for(int i=0; i<result.size(); i++){
                Object[] objects = (Object[])result.get(i);
                Role data = new Role((Integer)objects[0],objects[1].toString());

                Set<Constraints> constraintSet = new HashSet<Constraints>();
                Query constraintQuery = hibernateSession.createNativeQuery("call p_getAllConstraintByRoleId(?)");
                constraintQuery.setParameter(1,data.getRoleId());
                List constraintResult = constraintQuery.list();
                for (int j=0 ; j < constraintResult.size() ; j++) {
                    Object[] constraintObjects = (Object[])constraintResult.get(j);
                    Constraints constraint = new Constraints(
                            (Integer)constraintObjects[0]
                            ,constraintObjects[1].toString());
                    constraintSet.add(constraint);

                }
                data.setConstraint(constraintSet);
                roles.add(data);
            }

            return roles;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }


}