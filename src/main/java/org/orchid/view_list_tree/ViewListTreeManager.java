package org.orchid.view_list_tree;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.json.JSONObject;
import org.orchid.models.Tree;
import org.orchid.utils.HibernateUtil;

import java.util.ArrayList;
import java.util.List;

public class ViewListTreeManager {
    private Tree tree;

    public Tree getTree() {
        return tree;
    }

    public void setTree(Tree tree) {
        this.tree = tree;
    }

    public ViewListTreeManager () {
        this.tree = new Tree();
    }

    public ArrayList<Tree> finAllTree () {
        ArrayList<Tree> arrTree = new ArrayList<Tree>();
        String hql = "FROM  Tree";
        Session hibernateSession = HibernateUtil.getSession();
        try {
            Query query = hibernateSession.createQuery(hql);
            List result = query.list();
            for (int i = 0 ; i < result.size() ; i++) {
                arrTree.add((Tree)result.get(i));
            }
            hibernateSession.close();
            return arrTree;
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
            hibernateSession.beginTransaction().rollback();
        }
        return null;
    }

}
