package org.orchid.view_list_tree;

import org.json.JSONArray;
import org.json.JSONObject;
import org.orchid.models.Tree;
import org.orchid.view_tree_details.ViewTreeDetailsManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "ViewListTreeServlet")
public class ViewListTreeServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        ViewListTreeManager manager = new ViewListTreeManager();
        ArrayList<Tree> treeArr = (ArrayList<Tree>) manager.finAllTree();
        JSONArray jsonArr = new JSONArray();
        for (Tree tree :  treeArr) {
            JSONObject json = ViewTreeDetailsManager.toJson(tree);
            jsonArr.put(json);
        }
        response.getWriter().append(jsonArr.toString());
    }
}
