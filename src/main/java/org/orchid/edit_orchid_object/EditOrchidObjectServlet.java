package org.orchid.edit_orchid_object;

import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "EditOrchidObjectServlet")
public class EditOrchidObjectServlet extends HttpServlet {
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        EditOrchidObjectManager manager = new EditOrchidObjectManager();
        JSONObject json = new JSONObject();
        boolean result = false;
        try {
            int treeId = Integer.parseInt(request.getParameter("treeId"));
            int orchidObjectId = Integer.parseInt(request.getParameter("orchidObjectId"));
            int orchidId = Integer.parseInt(request.getParameter("selectOrchidSpecies"));

            manager.getOrchidObject().getTree().setTreeId(treeId);
            manager.getOrchidObject().getOrchid().setOrchidId(orchidId);
            manager.getOrchidObject().setOrchidObjectId(orchidObjectId);
            result = manager.updateOrchid();
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
        json.put("result",result);
        response.getWriter().append(json.toString());
    }
}
