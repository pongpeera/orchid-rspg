package org.orchid.edit_orchid_object;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.orchid.models.OrchidObject;
import org.orchid.utils.HibernateUtil;

public class EditOrchidObjectManager {

    private OrchidObject orchidObject;

    public EditOrchidObjectManager () {
        this.orchidObject = new OrchidObject();
    }

    public OrchidObject getOrchidObject() {
        return orchidObject;
    }

    public void setOrchidObject(OrchidObject orchidObject) {
        this.orchidObject = orchidObject;
    }

    public boolean updateOrchid () {
        Session hibernateSession = HibernateUtil.getSession();
        try {
            hibernateSession.update(this.orchidObject);
            hibernateSession.beginTransaction().commit();
            hibernateSession.close();
            return true;
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
        return false;
    }
}
