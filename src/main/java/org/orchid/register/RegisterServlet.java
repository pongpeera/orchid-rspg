package org.orchid.register;

import org.json.JSONObject;
import org.orchid.register.RegisterManager;
import org.orchid.models.Login;
import org.orchid.models.Member;
import org.orchid.utils.OrchidUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "RegisterServlet")
public class RegisterServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        JSONObject json = new JSONObject();
        boolean result;
        try {
            String email = request.getParameter("email");
            String password = request.getParameter("password");
            String personalId = request.getParameter("personalId");
            String name = request.getParameter("name");
            String telephoneNumber = request.getParameter("telephoneNumber");
            String gender = request.getParameter("gender");
            String address = request.getParameter("address");
            String birthDate = request.getParameter("birthDate");
            String postId = request.getParameter("postId");

            Login login = new Login();
            login.setEmail(email);
            login.setPassword(password);
            login.getPerson().setPersonalId(personalId);
            login.getPerson().setName(name);
            login.getPerson().setTelephoneNumber(telephoneNumber);
            login.getPerson().getRole().add(new Member(gender,address,birthDate,postId,personalId));
            RegisterManager manager = new RegisterManager(login);
            result = manager.registerMember();
            json.put("result",result);

        } catch (NullPointerException e) {
            System.out.println(e.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        response.getWriter().append(json.toString());
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        OrchidUtil.goTo("/templates/register.jsp",request,response,getServletContext());
    }



}
