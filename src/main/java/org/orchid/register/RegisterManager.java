package org.orchid.register;

import org.hibernate.Session;
import org.orchid.models.Login;
import org.orchid.models.Role;
import org.orchid.utils.HibernateUtil;

public class RegisterManager {
    private Login login;

    public RegisterManager () {

    }
    public RegisterManager (Login login) {
        this.login = login;
    }

    public boolean registerMember () {
        Session hibernateSession = HibernateUtil.getSession();
        try {
            hibernateSession.save(this.login.getPerson());
            hibernateSession.save(this.login);
            for (Role r : this.login.getPerson().getRole()) {
                hibernateSession.save(r);
            }
            hibernateSession.beginTransaction().commit();
            hibernateSession.close();
            return true;
        } catch (Exception e ) {
            hibernateSession.beginTransaction().rollback();
            hibernateSession.close();
            System.out.println(e.getMessage());
        }
        return false;
    }
}
