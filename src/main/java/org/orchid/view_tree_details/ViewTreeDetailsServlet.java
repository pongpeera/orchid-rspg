package org.orchid.view_tree_details;

import org.json.JSONObject;
import org.orchid.models.Tree;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ViewTreeDetailsServlet")
public class ViewTreeDetailsServlet extends HttpServlet {


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        ViewTreeDetailsManager manager = new ViewTreeDetailsManager();
        try {
            int id = Integer.parseInt(request.getParameter("treeId"));
            manager.getTree().setTreeId(id);
            Tree tree = manager.findTreeById();
            JSONObject json = manager.toJson(tree);
            response.getWriter().append(json.toString());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
