package org.orchid.view_tree_details;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.json.JSONObject;
import org.orchid.models.Tree;
import org.orchid.utils.HibernateUtil;

import java.util.List;

public class ViewTreeDetailsManager {
    private Tree tree;

    public ViewTreeDetailsManager () {
        this.tree = new Tree();
    }

    public Tree getTree() {
        return tree;
    }

    public void setTree(Tree tree) {
        this.tree = tree;
    }

    public Tree findTreeById() {
        String hql = "FROM  Tree  WHERE treeId = :treeId";
        Session hibernateSession = HibernateUtil.getSession();
        try {
            Query query = hibernateSession.createQuery(hql);
            query.setParameter("treeId",this.tree.getTreeId());
            List result = query.list();
            hibernateSession.close();
            return (Tree)result.get(0);
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
            hibernateSession.beginTransaction().rollback();
        }
        return null;
    }

    public static JSONObject toJson(Tree tree) {
        JSONObject jsonTree = new JSONObject();
        JSONObject jsonGarden = new JSONObject();
        JSONObject jsonTreeSpecies = new JSONObject();

        jsonTree.put("id",tree.getTreeId());
        jsonTree.put("latitude",tree.getLatitude());
        jsonTree.put("longitude",tree.getLongitude());

        jsonGarden.put("id",tree.getGarden().getGardenId());
        jsonGarden.put("name",tree.getGarden().getLocation());
        jsonTreeSpecies.put("id",tree.getTreeSpecies().getTreeSpeciesId());
        jsonTreeSpecies.put("name",tree.getTreeSpecies().getTreeSpeciesName());

        jsonTree.put("garden",jsonGarden);
        jsonTree.put("species",jsonTreeSpecies);
        return jsonTree;
    }

}
