package org.orchid.view_list_orchid_object;

import org.json.JSONArray;
import org.json.JSONObject;
import org.orchid.models.OrchidObject;
import org.orchid.view_orchid_object_details.ViewOrchidObjectManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "ViewListOrchidObjectServlet")
public class ViewListOrchidObjectServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        ViewListOrchidObjectManager manager = new ViewListOrchidObjectManager();
        JSONArray jsonArray = new JSONArray();
        try {
            ArrayList<OrchidObject> orchidObjects = manager.findAllOrchidObject();
            for (OrchidObject orchidObject : orchidObjects) {
                JSONObject json = ViewOrchidObjectManager.toJson(orchidObject);
                jsonArray.put(json);
            }
            response.getWriter().append(jsonArray.toString());
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }
}
