package org.orchid.view_list_orchid_object;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.orchid.models.OrchidObject;
import org.orchid.utils.HibernateUtil;

import java.util.ArrayList;
import java.util.List;

public class ViewListOrchidObjectManager {
    private OrchidObject orchidObject;

    public ViewListOrchidObjectManager() {
        this.orchidObject = new OrchidObject();
    }

    public OrchidObject getOrchidObject() {
        return orchidObject;
    }
    public ArrayList<OrchidObject> findAllOrchidObject () {
        ArrayList<OrchidObject> arrOrchidObject = new ArrayList<OrchidObject>();
        String hql = "FROM  OrchidObject";
        Session hibernateSession = HibernateUtil.getSession();
        try {
            Query query = hibernateSession.createQuery(hql);
            List result = query.list();
            for (int i = 0 ; i < result.size() ; i++) {
                arrOrchidObject.add((OrchidObject)result.get(i));
            }
            hibernateSession.close();
            return arrOrchidObject;
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
            hibernateSession.beginTransaction().rollback();
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
        return null;
    }
}
