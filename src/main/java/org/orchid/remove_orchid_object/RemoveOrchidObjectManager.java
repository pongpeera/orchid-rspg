package org.orchid.remove_orchid_object;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.orchid.models.OrchidObject;
import org.orchid.models.OrchidObjectRemoveLog;
import org.orchid.utils.HibernateUtil;

public class RemoveOrchidObjectManager {
    private OrchidObject orchidObject;

    public RemoveOrchidObjectManager() {
        this.orchidObject = new OrchidObject();
    }

    public OrchidObject getOrchidObject() {
        return orchidObject;
    }


    public OrchidObjectRemoveLog removeLog;

    public OrchidObjectRemoveLog getRemoveLog() {
        return removeLog;
    }

    public void setRemoveLog(OrchidObjectRemoveLog removeLog) {
        this.removeLog = removeLog;
    }

    public boolean insertRemoveReason () {
        Session hibernateSession = HibernateUtil.getSession();
        try {
            hibernateSession.save(this.removeLog);
            hibernateSession.beginTransaction().commit();
            hibernateSession.close();
            return true;
        } catch (HibernateException e) {
            System.out.print(e.getMessage());
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
        return false ;
    }

    public boolean deleteOrchid () {

        Session hibernateSession = HibernateUtil.getSession();
        Transaction transaction = hibernateSession.beginTransaction();
        String hql = "DELETE OrchidObject WHERE orchidObjectId = :orchidObjectId";
        try {
            Query query = hibernateSession.createQuery(hql);
            query.setParameter("orchidObjectId",this.orchidObject.getOrchidObjectId());
            query.executeUpdate();
            transaction.commit();
            hibernateSession.close();
            return true;
        } catch (HibernateException e) {
            System.out.print(e.getMessage());
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
        return false ;
    }
}
