package org.orchid.remove_orchid_object;

import org.json.JSONObject;
import org.orchid.models.OrchidObjectRemoveLog;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "RemoveOrchidObjectServlet")
public class RemoveOrchidObjectServlet extends HttpServlet {
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        RemoveOrchidObjectManager manager = new RemoveOrchidObjectManager();
        JSONObject json = new JSONObject();
        boolean isDeleted = false;
        try {
            int orchidObjectId = Integer.parseInt(request.getParameter("orchidObjectId"));

            manager.getOrchidObject().setOrchidObjectId(orchidObjectId);
            isDeleted = manager.deleteOrchid();

            if (isDeleted &&request.getParameter("description") != null && request.getParameter("removeDate") != null) {
                String reason = request.getParameter("description");
                String removeDate = request.getParameter("removeDate");
                OrchidObjectRemoveLog removeLog = new OrchidObjectRemoveLog();
                removeLog.setRemoveDate(Integer.toString(orchidObjectId));
                removeLog.setDescription(reason);
                removeLog.setRemoveDate(removeDate);
                manager.setRemoveLog(removeLog);
                manager.insertRemoveReason();
            }

        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
        json.put("result",isDeleted);
        response.getWriter().append(json.toString());
    }
}
