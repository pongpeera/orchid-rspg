package org.orchid.login;

import org.json.JSONObject;
import org.orchid.login.LoginManager;

import org.orchid.utils.OrchidUtil;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "LoginServlet")
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        boolean result = false;
        JSONObject json = new JSONObject();
        try {
            String email = request.getParameter("email");
            String password = request.getParameter("password");
            LoginManager manager = new LoginManager();
            manager.getLogin().setEmail(email);
            manager.getLogin().setPassword(password);
            result = manager.verifyLogin();
            if (!result) {
                json.put("resultMessage","ไม่สามารถเข้าสู่ระบบได้");
            } else {
                HttpSession session = request.getSession(true);
                session.setAttribute("authentication",manager.generateToken());
            }

        } catch (NullPointerException e) {
            System.out.print("login data is null");
        } catch (Exception e) {
            System.out.println("other exception");
            System.out.println(e.getMessage());
        }

        json.put("loginResult",result);
        response.getWriter().append(json.toString());
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String destination = (session.getAttribute("authentication") != null) ? "/templates/dashboard.jsp" : "/templates/login.jsp";
        OrchidUtil.goTo(destination,request,response,getServletContext());
    }

    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        session.invalidate();
    }


}

