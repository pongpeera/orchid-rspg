package org.orchid.login;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.orchid.models.Login;
import org.orchid.utils.HibernateUtil;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.List;


public class LoginManager {
    private Login login;


    public LoginManager() {
        this.login = new Login();
    }


    public Login getLogin() {
        return login;
    }

    public void setLogin(Login login) {
        this.login = login;
    }

    public boolean verifyLogin () {
        String hql = "FROM  Login  WHERE email = :email";
        Session hibernateSession = HibernateUtil.getSession();
        try {
            Query query = hibernateSession.createQuery(hql);
            query.setParameter("email",this.getLogin().getEmail());
            List result = query.list();
            hibernateSession.close();
            if (result != null) {
                Login loginData = (Login) result.get(0);
                return this.login.getPassword().equals(loginData.getPassword());
            }

        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
        return false;
    }

    public String generateToken () {
        SecureRandom random = new SecureRandom();
        return new BigInteger(130, random).toString(32);
    }
}

