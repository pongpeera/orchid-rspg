package org.orchid.view_orchid_details;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.json.JSONArray;
import org.json.JSONObject;
import org.orchid.models.Benefit;
import org.orchid.models.Orchid;
import org.orchid.models.OrchidStatus;
import org.orchid.utils.HibernateUtil;

import java.util.List;

public class ViewOrchidDetailsManager {
    private Orchid orchid;

    public ViewOrchidDetailsManager () {
        this.orchid = new Orchid();
    }

    public Orchid getOrchid() {
        return orchid;
    }

    public void setOrchid(Orchid orchid) {
        this.orchid = orchid;
    }
    public Orchid findOrchidById () {
        String hql = "FROM  Orchid  WHERE orchidId = :orchidId";
        Session hibernateSession = HibernateUtil.getSession();
        try {
            Query query = hibernateSession.createQuery(hql);
            query.setParameter("orchidId",this.getOrchid().getOrchidId());
            List result = query.list();
            hibernateSession.close();
            return (Orchid) result.get(0);
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
            hibernateSession.beginTransaction().rollback();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public static JSONObject toJson(Orchid orchid) {
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonStatusArray = new JSONArray();
        JSONArray jsonBenefitArray = new JSONArray();

        jsonObject.put("id",orchid.getOrchidId());
        jsonObject.put("localName" ,orchid.getLocalName());
        jsonObject.put("nameOfScience",orchid.getNameOfScience());
        jsonObject.put("flower",orchid.getMonthOfFlower());
        jsonObject.put("endOfFlower",orchid.getMonthOfEndFlower());

        for (OrchidStatus status : orchid.getOrchidStatus()) {
            JSONObject jsonStatus = new JSONObject();
            jsonStatus.put("id",status.getOrchidStatusId());
            jsonStatus.put("name",status.getOrchidStatusName());
            jsonStatusArray.put(jsonStatus);
        }

        for (Benefit benefit : orchid.getBenefits()) {
            JSONObject jsonBenefit = new JSONObject();
            jsonBenefit.put("id",benefit.getBenefitId());
            jsonBenefit.put("name",benefit.getBenefitName());
            jsonBenefitArray.put(jsonBenefit);
        }

        jsonObject.put("status",jsonStatusArray);
        jsonObject.put("benefit",jsonBenefitArray);
        return jsonObject;
    }
}