package org.orchid.view_orchid_details;

import org.json.JSONObject;
import org.orchid.models.Orchid;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ViewOrchidDetailsServlet")
public class ViewOrchidDetailsServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        ViewOrchidDetailsManager manager = new ViewOrchidDetailsManager();
        try {
            int id = Integer.parseInt(request.getParameter("orchidId"));
            manager.getOrchid().setOrchidId(id);
            Orchid orchid = manager.findOrchidById();
            JSONObject json = ViewOrchidDetailsManager.toJson(orchid);
            response.getWriter().append(json.toString());
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }
}
