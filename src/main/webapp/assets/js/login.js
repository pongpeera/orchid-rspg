angular.module('orchid.login',['orchid.ui.service','ngValidate'])
    .factory('Login',function ($http) {
        var prefixURL = '/dashboard';
        return {
            post : function (data) {
                return $http({
                    method : 'POST',
                    url : prefixURL,
                    params : data
                });
            },
            delete : function () {
                return $http({
                    method : 'DELETE',
                    url : prefixURL
                });
            }
        };
    })
    .config(function ($validatorProvider) {
        $validatorProvider.setDefaults({
            errorElement: 'div',
            errorClass: 'text-danger blink'
        });
        $validatorProvider.setDefaultMessages({
            required: "This field is required.",
            remote: "Please fix this field.",
            email: "Please enter a valid email address.",
            url: "Please enter a valid URL.",
            date: "Please enter a valid date.",
            dateISO: "Please enter a valid date (ISO).",
            number: "Please enter a valid number.",
            digits: "Please enter only digits.",
            creditcard: "Please enter a valid credit card number.",
            equalTo: "Please enter the same value again.",
            accept: "Please enter a value with a valid extension.",
            maxlength: $validatorProvider.format("Please enter no more than {0} characters."),
            minlength: $validatorProvider.format("Please enter at least {0} characters."),
            rangelength: $validatorProvider.format("Please enter a value between {0} and {1} characters long."),
            range: $validatorProvider.format("Please enter a value between {0} and {1}."),
            max: $validatorProvider.format("Please enter a value less than or equal to {0}."),
            min: $validatorProvider.format("Please enter a value greater than or equal to {0}.")
        });
    })
    .controller('LoginController',['$rootScope','$scope','Login','NotificationMessage',function ($rootScope,$scope,Login,NotificationMessage) {
        $rootScope.title = 'การเข้าสู่ระบบ';
        $scope.email = undefined;
        $scope.password = undefined;
        $scope.rememberMe = true;

        $scope.loginValidation = {
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true
                }
            }
        };

        $scope.verifyLogin = function (e,form) {
            e.preventDefault();
            if (!form.validate()  ||  !$scope.email || !$scope.password) {
                NotificationMessage.show({
                    message : 'กรุณากรอกข้อมูลให้ครบถ้วน',
                    className : 'danger'
                });
               return;
            }

            var data = {
                email : $scope.email,
                password : $scope.password,
                remember : $scope.rememberMe,
                checkbox  : []
            };
            Login.post(data)
                .then(function (response) {
                    if(response && response.data && !response.data.loginResult) {
                        NotificationMessage.show({
                            message : response.data.resultMessage,
                            className : 'danger'
                        });
                    } else {
                        window.location = '/dashboard';
                    }
                },errorHandle);
        };

    }])
;
function errorHandle (err) {
    console.log(err);
}