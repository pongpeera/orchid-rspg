angular.module('orchid.register',['orchid.api','orchid.ui.service'])
    .factory('Register',function (API) {
        var prefixedURL = '/register';
        return {
            post : function (data) {
                return API.post(prefixedURL,data);
            }
        };
    })
    .controller('RegisterController',['$rootScope','$scope','Register','OnLoadModal','NotificationMessage','ConfirmModalService',function ($rootScope,$scope,Register,OnLoadModal,NotificationMessage,ConfirmModalService) {
        $rootScope.title = "ลงทะเบียน";
        $scope.member = {
            gender : 0
        };
        var register = function (params) {
            OnLoadModal.open();
            Register.post(params)
                .then(function (response) {
                    OnLoadModal.close();
                    if (response) {
                        NotificationMessage.show({
                            message : 'บันทึกข้อมูลเรียบร้อยแล้ว',
                            className : 'success'
                        });
                    }

                }, function () {
                    OnLoadModal.close();
                    NotificationMessage.show({
                        message : 'ไม่สามารถบันทึกข้อมูลได้ กรุณาตรวจสอบข้อมูล',
                        className : 'danger'
                    });
                });
        };
        $scope.onSubmit = function (e,form) {
            e.preventDefault();
            var params = {
                email : $scope.member.email,
                password : $scope.member.password,
                personalId : $scope.member.personalId,
                name : $scope.member.name,
                gender : $scope.member.gender,
                address : $scope.member.address,
                birthDate : $scope.member.birthDate,
                postId  : $scope.member.postId,
                telephoneNumber : $scope.member.telephoneNumber
            };
            ConfirmModalService.open('ยืนยันการสมัครสมาชิก','pe-7s-close-circle')
                .then(function (response) {
                    if (response) register(params);
                });

        };

    }])
;