angular.module('orchid.api',[])
    .service('API',function ($http) {
        this.get = function (url) {
            return $http.get(url);
        };

        this.post = function (url,data) {
            var config = {
                method : 'POST',
                url : url,
                params: data
            };
            return $http(config);
        };

        this.put = function (url,data) {
            var config = {
                method : 'PUT',
                url : url,
                params : data
            };
            return $http(config);
        };

        this.delete = function (url,data) {
            var config = {
                method : 'DELETE',
                url : url,
                params : data
            };
            return $http(config);
        };
    })
;