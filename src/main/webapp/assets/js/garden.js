angular.module('orchid.garden',['orchid.api'])
    .factory('Garden',function (API) {
        var prefixedURL = '/api/garden';
        return {
            list : function () {
                return API.get('/list'+prefixedURL);
            },
            get : function (id) {
                return API.get(prefixedURL+'?gardenId'+id);
            }
        };
    })
;