
angular.module('orchid.backend.dashboard',['orchid.login'])
    .controller('ListMenuController',['$rootScope','$scope','$location','Login',function ($rootScope,$scope,$location,Login) {
        $rootScope.title = "กระดานจัดการข้อมูล";
        $scope.contentTitle = "กระดานจัดการข้อมูล";
        $scope.isActive = function (route) {
            return ($location.path()).contains(route);
        };

        $scope.onLogout = function () {
            Login.delete()
                .then(function (response) {
                    if (response) {
                        window.location = "/dashboard";
                    }
                });
        };
    }])
;