
angular.module('orchid.orchid',['ngTable','orchid.api','orchid.ui.service'])
    .factory('Orchid',function (API) {
        var prefixedURL = '/api/orchid';
        return {
            list : function () {
                return API.get('/list'+prefixedURL);
            },
            get : function (id) {
                return API.get('/get'+prefixedURL+'?orchidId='+id);
            },
            post : function (data) {
                return API.post('/post'+prefixedURL,data);
            },
            put : function (data) {
                return API.put('/put'+prefixedURL,data);
            },
            delete : function (data) {
                return API.delete('/delete'+prefixedURL,data);
            }
        };
    })
    .controller('ListOrchidController',['$rootScope','$scope','Orchid','ConfirmModalService','CustomModalService','NotificationMessage',function ($rootScope,$scope,Orchid,ConfirmModalService,CustomModalService,NotificationMessage) {
        $rootScope.title = $scope.$parent.contentTitle = 'จัดการข้อมูลพันธุ์กล้วยไม้';
        $scope.isEdit = false;
        $scope.orchids = [];
        $scope.selectedOrchid = undefined;
        $scope.orchidDetails = undefined;
        var fetchOrchid = function () {
            Orchid.list()
                .then(function (response) {
                    if (response && response.data && response.data.length > 0) {
                        $scope.orchids = response.data;
                    }
                });
        };

        $scope.removeOrchid = function (id) {
            var params = {
                orchidId : id
            };
            Orchid.delete(params)
                .then(function (response) {
                    if (response) {
                        NotificationMessage.show({
                            message : 'ลบข้อมูลเรียบร้อยแล้ว',
                            className : 'success'
                        });
                        $scope.selectedOrchid = undefined;
                        $scope.orchidDetails = undefined;
                        fetchOrchid();
                    }
                },errorHandle);
        };

        $scope.confirmRemoveOrchid = function () {
            var orchid = $scope.selectedOrchid;
            ConfirmModalService.open('คุณต้องการลบกล้วยไม้หมายเลข : '+orchid.id+'ใช่หรือไม่','pe-7s-close-circle')
                .then(function (response) {
                    if (response) $scope.removeOrchid(orchid.id);
                });
        };

        $scope.onModalOpen = function () {
            var config = {};
            CustomModalService.open(config)
                .then(function (response) {
                    if (response && response.data) {

                    }
                },errorHandle);

        };

        $scope.fetchOrchidDetails = function () {
            Orchid.get($scope.selectedOrchid.id)
                .then(function (response) {
                    if (response && response.data) {
                        $scope.orchidDetails = response.data;
                    }
                },errorHandle);
        };

        $scope.$watch('selectedOrchid',function (newVal,oldVal) {
            if (newVal) {
                $scope.fetchOrchidDetails();
            }
        });

        fetchOrchid();
    }])
    .controller('AddOrchidController',['$rootScope','$scope','Orchid','$filter','NotificationMessage','OnLoadModal','$location',function ($rootScope,$scope,Orchid,$filter,NotificationMessage,OnLoadModal,$location) {
        $rootScope.title = $scope.$parent.contentTitle = 'เพิ่มข้อมูลพันธุ์กล้วยไม้';1
        $scope.months = moment.months();
        $scope.status = [
            {
                id : 0,
                name : 'ใกล้สูญพันธุ์',
                selected : false
            },
            {
                id : 1,
                name : 'เฉพาะถิ่น',
                selected : false
            },
            {
                id : 2,
                name : 'หายาก',
                selected : false
            }
        ];

        $scope.benefit = [
            {
                id : 0,
                name : 'กลิ่นหอม',
                selected : false
            },
            {
                id : 1,
                name : 'สมุนไพร',
                selected : false
            }
        ];
        $scope.orchid = {};


        $scope.addOrchid = function (form) {
            if (!form.validate()) {
                return;
            }
            var params = {
                orchidId : $scope.orchid.id,
                nameOfScience : $scope.orchid.nameOfScience,
                localName : $scope.orchid.localName,
                flower : $scope.orchid.flower,
                endFlower : $scope.orchid.endFlower,
                status :$scope.orchid.status,
                benefit : $scope.orchid.benefit
            };
            OnLoadModal.open();
            Orchid.post(params)
                .then(function (response) {
                    var notificationConf = {
                        message : 'ไม่สามารถบันทึกข้อมูลได้เนื่องจากมีหมายซ้ำซ้อน' || response.data.message,
                        className : 'danger'
                    };

                    if (response && response.data && response.data.completed) {
                        OnLoadModal.close();
                        notificationConf.message = 'บันทึกข้อมูลเรียบร้อยแล้ว';
                        notificationConf.className = 'success';
                    }
                    NotificationMessage.show(notificationConf);
                    $location.path('/staff/orchid');

                },errorHandle);
        };

        $scope.selectStatus = function () {
            var status = $filter('filter')($scope.status,{selected : true});
            var selectedStatus = [];
            angular.forEach(status,function (val,key) {
                if (val.selected == true) {
                    selectedStatus.push(val.id);
                }
            });
            $scope.orchid.status = selectedStatus;
        };

        $scope.selectBenefit = function () {
            var benefit = $filter('filter')($scope.benefit,{selected : true});
            var selectedBenefit = [];
            angular.forEach(benefit,function (val,key) {
                if (val.selected == true) {
                    selectedBenefit.push(val.id);
                }
            });

            $scope.orchid.benefit = selectedBenefit;
        };

    }])
    .controller('EditOrchidController',['$rootScope','$scope','Orchid','$routeParams','$filter','$location',function ($rootScope,$scope,Orchid,$routeParams,$filter,$location) {
        $rootScope.title = $scope.$parent.contentTitle = 'แก้ไขข้อมูลพันธุ์กล้วยไม้';
        $scope.months = moment.months();
        $scope.orchid = {};
        $scope.status = [
            {
                id : 0,
                name : 'ใกล้สูญพันธุ์',
                selected : false
            },
            {
                id : 1,
                name : 'เฉพาะถิ่น',
                selected : false
            },
            {
                id : 2,
                name : 'หายาก',
                selected : false
            }
        ];

        $scope.benefit = [
            {
                id : 0,
                name : 'กลิ่นหอม',
                selected : false
            },
            {
                id : 1,
                name : 'สมุนไพร',
                selected : false
            }
        ];

        $scope.selectStatus = function () {
            var status = $filter('filter')($scope.status,{selected : true});
            var selectedStatus = [];
            angular.forEach(status,function (val,key) {
                if (val.selected == true) {
                    selectedStatus.push(val.id);
                }
            });
            $scope.orchidDetails.status = selectedStatus;
        };

        $scope.selectBenefit = function () {
            var benefit = $filter('filter')($scope.benefit,{selected : true});
            var selectedBenefit = [];
            angular.forEach(benefit,function (val,key) {
                if (val.selected == true) {
                    selectedBenefit.push(val.id);
                }
            });

            $scope.orchidDetails.benefit = selectedBenefit;
        };

        $scope.updateOrchid = function () {
            var params = {
                orchidId : $scope.orchidDetails.id,
                nameOfScience : $scope.orchidDetails.nameOfScience,
                localName : $scope.orchidDetails.localName,
                flower : $scope.orchidDetails.flower,
                endFlower : $scope.orchidDetails.endOfFlower,
                status :$scope.orchidDetails.status,
                benefit : $scope.orchidDetails.benefit
            };
        console.log(params);
            Orchid.put(params)
                .then(function (response) {
                    if (response) {

                    }
                },errorHandle);
        };

        Orchid.get($routeParams.id)
            .then(function (response) {
                if (response && response.data) {
                    $scope.orchidDetails = response.data;
                    angular.forEach($scope.orchidDetails.benefit,function (val) {
                        for (var i=0 ; i < $scope.benefit.length ; i++ ) {
                            if (val.id == $scope.benefit[i].id) {
                                $scope.benefit[i].selected = true;
                            }
                        }
                    });


                    angular.forEach($scope.orchidDetails.status,function (val) {
                        for (var i=0 ; i < $scope.status.length ; i++ ) {
                            if (val.id == $scope.status[i].id) {
                                $scope.status[i].selected = true;
                            }
                        }
                    });

                    $scope.selectStatus();
                    $scope.selectBenefit();
                }
            },errorHandle);



        // $scope.ok = function () {
        //     $scope.updateOrchid().then(function (response) {
        //         if (response && response.data) {
        //             $uibModalInstance.close(response.data);
        //         }
        //
        //     },errorHandle);
        // };
        //
        // $scope.cancel = function () {
        //     $uibModalInstance.dismiss('cancel');
        // };
    }])
  /*  .controller('orchidDetailsController',['$rootScope','$scope','$routeParams','Orchid',function ($rootScope,$scope,$routeParams,Orchid) {
        $rootScope.title = 'รายละเอียดข้อมูลพันธุ์กล้วยไม้';
        $scope.orchidDetails = undefined;
        $scope.fetchTreeDetails = function () {
            Orchid.get($routeParams.orchidId)
                .then(function (response) {
                    if (response && response.data) {
                        $scope.orchidDetails = response.data;
                    }
                },errorHandle);
        };
    }])*/
;
