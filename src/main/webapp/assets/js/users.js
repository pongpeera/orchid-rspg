angular.module('orchid.backend.users',['orchid.api','ui.bootstrap','orchid.ui.service','orchid.constants','orchid.ui.directives'])
    .factory('Role',function (API) {
        var prefixedURL = '/api/role';
        return {
            list : function () {
                return API.get('/list'+prefixedURL);
            },
            get : function (email) {
                return API.get('/get'+prefixedURL+"?email="+email);
            },
            post : function (data) {
                return API.post('/post'+prefixedURL,data);
            },
            put : function (data) {
            return API.put('/put'+prefixedURL,data);
        }
        };
    })
    .factory('Constraint',function (API) {
        var prefixedURL = '/api/constraint';
        return {
            get : function (email) {
                return API.get('/get'+prefixedURL+"?email="+email);
            },
            post: function (data) {
                return API.post('/post'+prefixedURL, data);
            },
            put : function (data) {
                return API.put('/put'+prefixedURL,data);
            }
        }
    })
    .factory('User',function (API) {
        var prefixedURL = '/api/user';
        return {
            list : function () {
                return API.get('/list'+prefixedURL);
            },
            post : function (data) {
                return API.post('/post'+prefixedURL,data);
            },
            put : function (data) {
                return API.put('/put'+prefixedURL,data);
            },
            delete : function (data) {
                return API.delete('/delete'+prefixedURL,data);
            }
        };
    })
    .controller('CreateUserController',['$rootScope','$scope','Role','User','ConfirmModalService','NotificationMessage','OnLoadModal','$location',function ($rootScope,$scope,Role,User,ConfirmModalService,NotificationMessage,OnLoadModal,$location) {
        $rootScope.title = $scope.$parent.contentTitle = "เพิ่มผู้ใช้ใหม่";
        $scope.roles = [];
        var require = {required : true};
        $scope.createUserValidations = {
            rules : {
                email : {
                    required : true,
                    email : true
                },
                password :  {
                    required : true,
                    minlength : 4,
                    maxlength : 30
                },
                role : require,
                name : require,
                personalId : {
                    required : true,
                    minlength : 13,
                    maxlength : 13
                },
                telephoneNumber : {
                    required : true,
                    minlength : 10,
                    maxlength : 10
                },
                department : require
            }
        };
        $scope.user = {};

        $scope.user.imageSrc = undefined;

        $scope.confirmAddUser = function (e,form) {
            e.preventDefault();
            if (!form.validate()) {
                NotificationMessage.show({
                    message : 'กรุณากรอกข้อมูลให้ครบถ้วน'
                });
                return;
            }
            var params = {
                email : $scope.user.email,
                password : $scope.user.password,
                selectedRole : $scope.user.selectedRole,
                name : $scope.user.name,
                telephoneNumber : $scope.user.telephoneNumber,
                department : $scope.user.department,
                personalId : $scope.user.personalId
            };
            var modalInstance = ConfirmModalService.open();
            modalInstance.then(function (isConfirm) {
                if (isConfirm) {
                    OnLoadModal.open();
                    User.post(params)
                        .then(function (response) {
                            var message = {
                                message : 'ไม่สามารถเพิ่มเจ้าหน้าที่ได้',
                                className : 'danger'
                            };
                            if(response && response.data && response.data.result) {
                                message = {
                                    message : 'บันทึกข้อมูลเรียบร้อยแล้ว',
                                    className : 'success'
                                };
                            }
                            OnLoadModal.close();
                            NotificationMessage.show(message);
                            $location.path('/manage_user');
                        },errorHandle);
                }
            },function () {/*Modal dismiss*/});
        };

        var fetchRoles = function () {
            Role.list()
                .then(function (response) {
                    if(response && response.data) {
                        $scope.roles = response.data;
                        $scope.user.selectedRole = $scope.roles[0].id;
                    }
                },errorHandle);
        };
        fetchRoles();
    }])
    .controller('ManageUserController',['$rootScope','$scope','ConfirmModalService','Role','Constraint','User','OnLoadModal','CustomModalService','NotificationMessage',function ($rootScope,$scope,ConfirmModalService,Role,Constraint,User,OnLoadModal,CustomModalService,NotificationMessage) {
        $rootScope.title = $scope.$parent.contentTitle = "จัดการข้อมูลผู้ใช้";
        $scope.selectedUser = undefined;
        $scope.selectedRole = undefined;
        $scope.userObject = [];
        $scope.roles = [];

        $scope.confirmRemoveUser = function () {
            ConfirmModalService.open('ยืนยันการลบผู้ใช้','pe-7s-close-circle')
                .then(function (response) {
                    if (response) {
                        User.delete({email : $scope.selectedUser.email})
                            .then(function (response) {
                                if (response) {
                                    NotificationMessage.show({
                                        message : 'ลบข้อมูลผู้ใช้เรียบร้อยแล้ว',
                                        className : 'success'
                                    });
                                    $scope.selectedUser = undefined;
                                }
                            },errorHandle);
                    }
                },function (dismiss) {/*dismiss message*/});
        };
        var fetchUsers = function () {
            User.list()
                .then(function (response) {
                    if (response && response.data) {
                        $scope.userObject = [];
                        angular.forEach(response.data,function (data,index) {
                            var obj = {
                                name : data.name,
                                email : data.email,
                                telephone : data.telephoneNumber,
                                department : data.department,
                                roles : data.roles
                            };

                            $scope.userObject.push(obj);

                        });
                    }
                },errorHandle);
        };
        $scope.assignRole = function () {
            var roleTemp = [];
            for (var i = 0 ; i < $scope.roles.length ; i++ ) {
                if ($scope.roles[i].wasRole) {
                    roleTemp.push($scope.roles[i].id);
                }
            }
            var params = {
                email : $scope.selectedUser.email,
                roles : roleTemp
            };
            if (params.roles.length <=0) return;

            OnLoadModal.open();
            Role.put(params).then(function (response) {
                if (response) {
                    OnLoadModal.close();
                }
            },errorHandle);
        };

        var bindRoleData = function (selectIndex) {

            if (selectIndex != undefined) {

                var userIndex = _.findIndex($scope.selectedUser.roles, function (o) {
                    var id = $scope.roles[selectIndex].id;
                    return o.id == id;
                });

                if (userIndex >= 0) {
                    var rolesConstraint = $scope.roles[selectIndex].constraints;
                    for (var i = 0; i < rolesConstraint.length; i++) {
                        var userRoleConstraint = $scope.selectedUser.roles[userIndex].constraints;
                        for (var j = 0; j < userRoleConstraint.length; j++) {
                            var userConstraint = userRoleConstraint[j];
                            if (userConstraint.id == rolesConstraint[i].id) {
                                $scope.roles[selectIndex].constraints[i].haveConstraint = true;
                            }
                        }
                    }
                }

            }
        };
        $scope.$watch('selectedRole',bindRoleData);

        $scope.updateConstraint = function (index) {
            $scope.selectedRole = index;
            CustomModalService.open({
                templateUrl : 'update_constraint.jsp',
                size : 'sm',
                controller : 'UpdateConstraintController',
                data : {
                    email : $scope.selectedUser.email,
                    role : $scope.roles[$scope.selectedRole]
                }
            });
        };

        var fetchRoles = function () {
            Role.list()
                .then(function (response) {
                    if(response && response.data) {
                        $scope.roles = response.data;
                        fetchUsers();
                    }
                },errorHandle);
        };


        $scope.$watch('selectedUser',function (val) {
            if (val) {
                for (var j=0 ; j < $scope.roles.length ; j++) {
                    $scope.roles[j].wasRole = false;
                    for (var i=0 ; i < val.roles.length ; i++) {
                        var role = val.roles[i];
                        bindRoleData(j);
                        if (role.id == $scope.roles[j].id) {
                            $scope.roles[j].wasRole = true;
                        }
                    }


                }
            }

        });
        fetchRoles();
    }])
    .controller('UpdateConstraintController',['$rootScope','$scope','$uibModalInstance','data','Constraint','OnLoadModal',function ($rootScope,$scope,$uibModalInstance,data,Constraint,OnLoadModal) {
        $scope.role = data.role;
        $scope.ok = function (form) {
            var constraintTemp = [];
            for (var i=0 ; i < $scope.role.constraints.length ; i++) {
                if ($scope.role.constraints[i].haveConstraint) {
                    constraintTemp.push($scope.role.constraints[i].id);
                }
            }
            var params = {
                email :  data.email,
                roleId : data.role.id,
                constraint :constraintTemp
            };

            OnLoadModal.open();
            Constraint.put(params)
                .then(function (response) {
                    if (response) {
                        OnLoadModal.close();
                        $scope.cancel();
                    }
                },errorHandle);

        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }])
;



