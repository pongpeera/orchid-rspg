angular.module('orchid.orchidObject',['orchid.api','orchid.orchid','orchid.tree','orchid.garden','ngTable'])
    .factory('OrchidObject',function (API) {
        var prefixedURL = '/api/orchidObject';
        return {
            list : function () {
                return API.get('/list'+prefixedURL);
            },
            get : function (id) {
                return API.get('/get'+prefixedURL+'?orchidObjectId='+id);
            },
            post : function (data) {
                return API.post('/post'+prefixedURL,data);
            },
            put : function (data) {
                return API.put('/put'+prefixedURL,data);
            },
            delete : function (data) {
                return API.delete('/delete'+prefixedURL,data);
            }
        };
    })
    .controller('ListOrchidObjectController',['$rootScope','$scope','OrchidObject','CustomModalService','NgTableParams',function ($rootScope,$scope,OrchidObject,CustomModalService,NgTableParams) {
        $scope.orchidObjects = [];

        var fetchOrchidObject = function () {
            OrchidObject.list()
                .then(function (response) {
                    if (response && response.data) {
                        $scope.orchidObjects = response.data;
                    }
                });
        };

        $scope.removeOrchidObject = function (orchidObject) {
            var params = {
                orchidObjectId : orchidObject.id,
                description : orchidObject.reason,
                removeDate : orchidObject.date
            };
            OrchidObject.delete(params)
                .then(function (response) {
                    if (response && response.data) {
                        console.log(55555555);
                        fetchOrchidObject();
                    }
                },errorHandle);
        };

        $scope.confirmRemoveOrchidObject = function (orchidObject) {
            CustomModalService.open({
                templateUrl : 'remove_orchidObject.jsp',
                controller : 'RemoveOrchidObjectController',
                size : 'sm',
                data : { id : orchidObject.id }
            });
        };

        $scope.$watch('orchidObjects',function (newVal,oldVla) {
            if (newVal) {
                $scope.tableParams = new NgTableParams({}, { dataset: $scope.orchidObjects});
            }
        });
        
        $rootScope.$on('fetchOrchidObject',function () {
            fetchOrchidObject();
        });

        fetchOrchidObject();
    }])
    .controller('AddOrchidObjectController',['$rootScope','$scope','$routeParams','OrchidObject','Orchid','Tree','NotificationMessage','$location',function ($rootScope,$scope,$routeParams,OrchidObject,Orchid,Tree,NotificationMessage,$location) {
        $rootScope.title = $scope.$parent.contentTitle = 'เพิ่มข้อมูลกล้วยไม้ใหม่';

        $scope.orchidSpecies = [];
        $scope.orchidObject = {};

        $scope.addOrchidObjectValidation = {
            rules : {
                id : { required : true }
            }
        };
        var fetchTree = function () {

            Tree.get($routeParams.treeId)
                .then(function (response) {
                    if (response && response.data) {
                        var orchidObj = response.data;
                        $scope.orchidObject.tree = {
                            id : orchidObj.id,
                            name : orchidObj.species.name,
                            garden : orchidObj.garden.name
                        }
                    }
                },errorHandle);
        };

        var fetchOrchidSpecies = function () {
            Orchid.list()
                .then(function (response) {
                    if (response && response.data) {
                        $scope.orchidSpecies = response.data;
                    }
                });
        };

        $scope.addOrchidObject = function (form) {
            if (!form.validate()) {
                return;
            }
            var params = {
                treeId : $scope.orchidObject.tree.id,
                selectOrchidSpecies : $scope.orchidObject.species,
                orchidObjectId : $scope.orchidObject.id

            };
            console.log(params);
            OrchidObject.post(params)
                .then(function (response) {
                    if (response) {
                        NotificationMessage.show({
                            message : 'บันทึกข้อมูลเรียบร้อยแล้ว',
                            className : 'success'
                        });
                        $location.path("/staff/object");
                    }
                },errorHandle);
        };

        fetchTree();
        fetchOrchidSpecies();

    }])
    .controller('EditOrchidObjectController',['$rootScope','$scope','OrchidObject','Orchid','Tree','$routeParams','NotificationMessage','OnLoadModal','$location',function ($rootScope,$scope,OrchidObject,Orchid,Tree,$routeParams,NotificationMessage,OnLoadModal,$location) {
        $rootScope.title = $scope.$parent.contentTitle = 'แก้ไขข้อมูลกอกล้วยไม้';
        $scope.orchidObject = {
            id : $routeParams.id,
            species : $routeParams.id,
            tree : $routeParams.id
        };

        $scope.editOrchidObjectValidation = {
            rules : {
                id : { required : true }
            }
        };

        $scope.orchidSpecies = [];

        var fetchOrchidObjectDetails = function () {
            OrchidObject.get($scope.orchidObject.id)
                .then(function (response) {
                    if (response && response.data) {
                        $scope.orchidObject = response.data;
                    }
                },errorHandle);
        };

        var fetchOrchidSpecies = function () {
            Orchid.list()
                .then(function (response) {
                    if (response && response.data) {
                        $scope.orchidSpecies = response.data;

                    }
                });
        };

        $scope.updateOrchidObject = function (form) {
            if (!form.validate()) {
                return;
            }

            var params = {
                treeId : $scope.orchidObject.tree.id,
                selectOrchidSpecies : $scope.orchidObject.orchid.id,
                orchidObjectId : $scope.orchidObject.id
            };

            OnLoadModal.open();
            console.log(55555);
            OrchidObject.put(params)
                .then(function (response) {
                    if (response) {
                        OnLoadModal.close();
                        NotificationMessage.show({
                            message : 'บันทึกข้อมูลเรียบร้อยแล้ว',
                            className : 'success'
                        });
                    }
                    $location.path('/staff/object');
                });
        };

        fetchOrchidObjectDetails();
        fetchOrchidSpecies();
    }])
    .controller('OrchidObjectDetailsController',['$rootScope','$scope','$routeParams','OrchidObject',function ($rootScope,$scope,$routeParams,OrchidObject) {
        $rootScope.title = $scope.$parent.contentTitle = 'รายละเอียดข้อมูลกล้วยไม้';
        $scope.orchidObjectDetails = {};

        OrchidObject.get($routeParams.id)
            .then(function (response) {
                if (response && response.data) {
                    $scope.orchidObjectDetails = response.data;
                }
            },errorHandle);
    }])
    .controller('RemoveOrchidObjectController',['$rootScope','$scope','$uibModalInstance','data','OrchidObject','NotificationMessage',function ($rootScope,$scope,$uibModalInstance,data,OrchidObject,NotificationMessage) {
        $scope.orchidObject = {
            id : data.id
        };

        $scope.hasReason = false;

        var removeOrchidObject = function () {
            var params = {
                orchidObjectId : $scope.orchidObject.id,
                description : $scope.orchidObject.reason,
                removeDate : $scope.orchidObject.removeDate+" | "+moment().valueOf()
            };
            console.log(params);
            OrchidObject.delete(params)
                .then(function (response) {
                    if (response /*&& response.data*/) {
                        console.log(response);
                        NotificationMessage.show({
                            message : 'ลบข้อมูลเรียบร้อยแล้ว',
                            className : 'success'
                        });

                        $rootScope.$broadcast('fetchOrchidObject');
                    }
                },errorHandle);
        };
        $scope.ok = function () {
            removeOrchidObject();
            $uibModalInstance.close();
        };

        $scope.cancel = function () {
            $uibModalInstance.close();
        };
    }])
;
