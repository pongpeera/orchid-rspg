angular.module('orchid.log',['orchid.api','ngTable','orchid.ui.directives'])
    .factory('Log',function (API) {
        var prefixedURL = '/api/removelog';
        return {
            list : function () {
                return API.get('/list'+prefixedURL);
            }
        };
    })
    .controller('RemoveLogController',['$rootScope','$scope','Log','NgTableParams','$filter','$q',function ($rootScope,$scope,Log,NgTableParams,$filter,$q) {
        $rootScope.title = 'ประวัติการลบข้อมูล';
        $scope.logType = 'TreeRemoveLog';

        $scope.fetchRemoveLog = function () {
            Log.list()
                .then(function (response) {
                    if (response && response.data && response.data.length > 0) {
                        $scope.treeLog = [];
                        $scope.orchidObjectLog = [];
                        angular.forEach(response.data,function (obj) {
                            if (obj.type == 'TreeRemoveLog') {
                                $scope.treeLog.push(obj);
                            } else {
                                $scope.orchidObjectLog.push(obj);
                            }
                        });
                    }
                },errorHandle);
        };
        $scope.$watch('treeLog',function (val) {
            if (val)
                $scope.treeLogTableParams = new NgTableParams({}, {
                    dataset: $scope.treeLog
                });
        });
        $scope.$watch('orchidObjectLog',function (val) {
            if (val)
                $scope.orchidObjectLogTableParams = new NgTableParams({}, {
                    dataset: $scope.orchidObjectLog
                });
        });
        $scope.fetchRemoveLog();
    }])
;