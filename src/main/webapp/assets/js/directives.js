angular.module('orchid.ui.directives',[])
    .directive('autoComplete', function () {
        return {
            restrict: 'E',
            scope: {
                arrayObject : '=',
                selectedItem : '='
            },
            templateUrl: '../assets/partials/autoComplete.jsp',
            link : function (scope) {
                scope.keyword = undefined;
                scope.suggestions = [];
                scope.selectedIndex = -1;
                scope.selectedItem = undefined;


                scope.findByKeyword = function(){
                    scope.suggestions = [];
                    var maxListLength = 0;
                    for(var i=0; i<scope.arrayObject.length; i++){
                        var keyword = scope.keyword;
                        var pushObject = false;
                        angular.forEach(scope.arrayObject[i],function (val) {
                            if (val && typeof val != 'object') {
                                val = val.toString();
                                if (val.contains(keyword) && !pushObject) {
                                    pushObject = true;
                                    scope.suggestions.push(scope.arrayObject[i]);
                                    maxListLength += 1;
                                }
                            }

                        });

                        if(maxListLength === 5){
                            break;
                        }

                    }
                };

                scope.selectAndHide = function (index) {
                    scope.selectedItem = scope.suggestions[index];
                    scope.keyword = undefined;
                    scope.suggestions=[];
                    scope.selectedIndex = -1;
                };

                scope.$watch('keyword',function (newVal,oldVal) {
                    if (!newVal) scope.suggestions=[];
                });
            }
        };
    })
    .directive('chooseImage', function () {
        return {
            restrict: 'E',
            scope: {
                src : '=',
                label : '@'
            },
            templateUrl: '../assets/partials/previewImage.jsp'
            ,
            link: function (scope, element, attributes) {

                element.bind("change", function (changeEvent) {
                    scope.$apply(function () {
                        scope.src = changeEvent.target.files[0];
                        var reader = new FileReader();

                        reader.onload = function(event) {
                            scope.image_source = event.target.result;
                            scope.$apply();
                        };

                        reader.readAsDataURL(scope.src);

                    });
                });
             }
        };
    })
;