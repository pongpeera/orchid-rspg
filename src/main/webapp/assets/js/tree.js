angular.module('orchid.tree',['orchid.api','orchid.ui.service','orchid.garden','ngTable','ngMap'])
    .factory('Tree',function (API) {
        var prefixedURL = '/api/tree';
        return {
            list : function () {
                return API.get('/list'+prefixedURL);
            },
            get : function (id) {
                return API.get('/get'+prefixedURL+'?treeId='+id);
            },
            post : function (data) {
                return API.post('/post'+prefixedURL,data);
            },
            put : function (data) {
                return API.put('/put'+prefixedURL,data);
            },
            delete : function (data) {
                return API.delete('/delete'+prefixedURL,data);
            }
        };
    })
    .factory('TreeSpecies',function (API) {
        var prefixedURL = '/api/species';
        return {
            list : function () {
                return API.get('/list/'+prefixedURL);
            }
        };
    })
    .controller('ListTreeController',['$rootScope','$scope','Tree','CustomModalService','NgTableParams','NotificationMessage',function ($rootScope,$scope,Tree,CustomModalService,NgTableParams,NotificationMessage) {
        $rootScope.title = $scope.$parent.contentTitle = 'จัดการข้อมูลต้นไม้';
        $scope.tableParams = undefined;
        $scope.listTree = [];


        $scope.fetchTree = function () {
            Tree.list()
                .then(function (response) {
                    $scope.listTree = [];
                    if (response && response.data && response.data.length) {
                        var obj = response.data;
                        $scope.listTree = [];
                        for (var i = 0 ; i < obj.length ; i++) {
                            $scope.listTree.push({
                                id : obj[i].id,
                                species : obj[i].species.name,
                                garden : obj[i].garden.name,
                                zone : 1
                            });
                        }
                    }
                },errorHandle);
        };

        $scope.confirmRemoveTree = function (tree) {
            CustomModalService.open({
                templateUrl : 'remove_tree.jsp',
                controller : 'RemoveTreeController',
                size : 'sm',
                data : { id : tree.id }
            });
        };


        $scope.onModalOpen = function () {
            CustomModalService.open({
                controller : 'AddTreeController',
                templateUrl : 'add_tree.jsp'
            });
        };

        $scope.$watch('listTree',function (newVal,oldVal) {
            if (newVal) {
                $scope.tableParams = new NgTableParams({}, { dataset: $scope.listTree});
            }
        });

        $rootScope.$on('fetchTreeData', function (event, data) {
            $scope.fetchTree();
        });

        $scope.fetchTree();
    }])
    .controller('AddTreeController',['$rootScope','$scope','Garden','TreeSpecies','$uibModalInstance','Tree','NotificationMessage','NgMap',function ($rootScope,$scope,Garden,TreeSpecies,$uibModalInstance,Tree,NotificationMessage,NgMap) {
        $rootScope.title = 'เพิ่มข้มูลต้นไม้ใหม่';
        $scope.positionPicker = [];
        $scope.positions = [];
        $scope.redPin = "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|34BA46";
        $scope.greenPin = "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|FE7569";
        $scope.gardens = [];
        $scope.treeSpecies = [];
        $scope.tree = {
            garden : 0,
            species : 0
        };
        $scope.zone = undefined;
        $scope.addTreeValidation = {
            rules : {
                longitude : {
                    required : true
                },
                latitude : {
                    required : true
                }
            }
        };

        var fetchGarden = function () {
            Garden.list()
                .then(function (response) {
                    if (response && response.data && response.data.length) {
                        $scope.gardens = response.data;
                    }
                },errorHandle);
        };

        var fetchTreeSpecies = function () {
            TreeSpecies.list()
                .then(function (response) {
                    if (response && response.data && response.data.length) {
                        $scope.treeSpecies = response.data;
                    }
                });
        };

        var fetchTreePositions = function () {
            Tree.list()
                .then(function (response) {
                    if (response && response.data && response.data.length > 0) {
                        var obj = response.data;
                        $scope.positions = [];
                        for (var i = 0 ; i < obj.length ; i++) {
                            $scope.positions.push({
                                lat : obj[i].latitude,
                                lng : obj[i].longitude
                            });
                        }
                    }
                },errorHandle);
        };

        $scope.addTree = function () {
            var params = {
                gardenId : $scope.tree.garden,
                speciesId : $scope.tree.species,
                latitude : $scope.tree.latitude,
                longitude : $scope.tree.longitude
                /*,zone : $scope.tree.zone*/
            };
            return Tree.post(params);
        };

        $scope.ok = function (form) {
            if (!form.validate()) {return;}
            $scope.addTree().then(function (response) {
                if (response && response.data && response.data.isSuccess) {
                    $uibModalInstance.close();
                    NotificationMessage.show({
                        message : 'บันทึกข้อมูลเรียบร้อยแล้ว',
                        className : 'success'
                    });
                    $rootScope.$broadcast('fetchTreeData');
                }
            },errorHandle);
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };


        $scope.placeMarker = function (e) {
            var ll = e.latLng;
            $scope.positionPicker = [ll.lat(),ll.lng()];
            $scope.tree.latitude = ll.lat();
            $scope.tree.longitude = ll.lng();
        };

        fetchGarden();
        fetchTreeSpecies();
        fetchTreePositions();
        NgMap.getMap();
    }])
    .controller('EditTreeController',['$rootScope','$scope','$routeParams','Garden','TreeSpecies','Tree','NotificationMessage','$location','OnLoadModal',function ($rootScope,$scope,$routeParams,Garden,TreeSpecies,Tree,NotificationMessage,$location,OnLoadModal) {
        $rootScope.title = $scope.$parent.contentTitle = 'แก้ไขข้อมูลต้นไม้';
        $scope.gardens = [];
        $scope.treeSpecies = [];
        $scope.tree = undefined;
        $scope.treeSpeciesId = undefined;
        $scope.treeLatitude = undefined;
        $scope.treeLongitude = undefined;
        $scope.zone = undefined;
        $scope.editTreeValidation =  {
            rules : {
                longitude : {
                    required : true
                },
                latitude : {
                    required : true
                }
            }
        };

        $scope.fetchTreeDetails = function () {
            Tree.get($routeParams.id)
                .then(function (response) {
                    if (response && response.data) {
                        $scope.tree = response.data;
                    }
                },errorHandle);
        };

        var fetchGarden = function () {
            Garden.list()
                .then(function (response) {
                    if (response && response.data && response.data.length) {
                        $scope.gardens = response.data;
                    }
                });
        };

        var fetchTreeSpecies = function () {
            TreeSpecies.list()
                .then(function (response) {
                    if (response && response.data && response.data.length) {
                        $scope.treeSpecies = response.data;
                    }
                });
        };

        $scope.updateTree = function (form) {
            if (!form.validate()) {
                return;
            }
            var params = {
                treeId : $routeParams.id,
                speciesId : $scope.tree.species.id,
                gardenId : $scope.tree.garden.id,
                latitude : $scope.tree.latitude,
                longitude : $scope.tree.longitude
               /* ,zone : $scope.zone*/

            };
            OnLoadModal.open();
            Tree.put(params)
                .then(function (response) {
                    if (response) {
                        OnLoadModal.close();
                        NotificationMessage.show({
                            message : 'แก้ไขข้อมูลเรียบร้อยแล้ว',
                            className : 'success'
                        });
                        $location.path('/staff/tree');
                    }
                },errorHandle);
        };

        $scope.ok = function () {
            $scope.updateTree().then(function (response) {
                $uibModalInstance.close(response);
            },errorHandle);

        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
        fetchGarden();
        fetchTreeSpecies();
        $scope.fetchTreeDetails();

    }])
    .controller('TreeDetailsController',['$rootScope','$scope','$routeParams','Tree',function ($rootScope,$scope,$routeParams,Tree) {
        $rootScope.title = 'รายละเอียดข้อมูลต้นไม้';

        $scope.treeDetails = {};
        Tree.get($routeParams.id)
            .then(function (response) {

                if (response && response.data) {
                    $scope.treeDetails = response.data;
                }
            },errorHandle);

    }])
    .controller('RemoveTreeController',['$rootScope','$scope','$uibModalInstance','data','Tree','NotificationMessage','OnLoadModal',function ($rootScope,$scope,$uibModalInstance,data,Tree,NotificationMessage,OnLoadModal) {
        $scope.tree = {
            id : data.id
        };

        $scope.hasReason = false;

        var removeTree = function () {
            var params = {
                treeId : $scope.tree.id,
                description : $scope.tree.reason,
                removeDate : $scope.tree.removeDate+" | "+moment().valueOf()
            };

            OnLoadModal.open();
            Tree.delete(params)
                .then(function (response) {
                    if (response && response.data && response.data.result) {
                        OnLoadModal.close();
                        NotificationMessage.show({
                            message : 'ลบข้อมูลเรียบร้อยแล้ว',
                            className : 'success'
                        });

                        $rootScope.$broadcast('fetchTreeData');
                    }
                },errorHandle);
        };
        $scope.ok = function () {
            removeTree();
            $uibModalInstance.close();
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }])
;
