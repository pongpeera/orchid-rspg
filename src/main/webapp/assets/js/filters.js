angular.module('orchid.filter',[])
    .filter('autoCompleteItem',function () {
        return function (val) {
            var text = '';
            angular.forEach(val,function (value) {
                if (!(typeof value == 'object')) {
                    text += value+' ';
                }
            });
            return text;
        };
    })
    .filter('removeDateFormat',function () {
        return function (val) {
            var rawDate = val.split(' | ')[0];
            return moment(rawDate).format('DD/MM/YYYY');
        }
    });