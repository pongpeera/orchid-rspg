
angular.module('orchid.backend',[
        'ngRoute','ui.bootstrap','ngAnimate','ngValidate',
        'orchid.constants','orchid.api','orchid.backend.users','orchid.backend.dashboard','orchid.ui.service',
        'orchid.ui.directives','orchid.orchidObject','orchid.tree','orchid.orchid','orchid.filter','orchid.log'
    ])
    .config(function($routeProvider,$validatorProvider) {
        $routeProvider
            .when("/create_user", {
                templateUrl : "./templates/create_user.jsp",
                controller : 'CreateUserController'
            })
            .when("/manage_user",{
                templateUrl : "./templates/manage_user.jsp",
                controller : 'ManageUserController'
            })
            .when("/staff/tree",{
                templateUrl : "./templates/manage_tree.jsp",
                controller : 'ListTreeController'
            })
            .when("/staff/tree/:id/edit",{
                templateUrl : "./templates/edit_tree.jsp",
                controller : 'EditTreeController'
            })
            .when("/staff/tree/:id",{
                templateUrl : "./templates/details_tree.jsp",
                controller : 'TreeDetailsController'
            })
            .when("/staff/orchid",{
                templateUrl : "./templates/manage_orchid.jsp",
                controller : 'ListOrchidController'
            })
            .when("/staff/orchid/:id/edit",{
                templateUrl : "./templates/edit_orchid.jsp",
                controller : 'EditOrchidController'
            })
            .when("/staff/orchid/add",{
                templateUrl : "./templates/add_orchid.jsp",
                controller : 'AddOrchidController'
            })
            .when("/staff/object",{
                templateUrl : "./templates/manage_orchid_object.jsp",
                controller : 'ListOrchidObjectController'
            })
            .when("/staff/object/:treeId/add",{
                templateUrl : "./templates/add_orchid_object.jsp",
                controller : 'AddOrchidObjectController'
            })
            .when("/staff/object/:id/edit",{
                templateUrl : "./templates/edit_orchid_object.jsp",
                controller : 'EditOrchidObjectController'
            })
            .when("/staff/object/:id",{
                templateUrl : "./templates/details_orchid_object.jsp",
                controller : 'OrchidObjectDetailsController'
            })
            .when("/staff/removelog",{
                templateUrl : "./templates/remove_log.jsp",
                controller : 'RemoveLogController'
            })
            .otherwise({
                templateUrl : "./templates/error/404.jsp"
            });

        $validatorProvider.setDefaults({
            errorElement: 'div',
            errorClass: 'text-danger blink'
        });
        $validatorProvider.setDefaultMessages({
            required: "กรุณากรอกข้อมูล",
            remote: "Please fix this field.",
            email: "Please enter a valid email address.",
            url: "Please enter a valid URL.",
            date: "Please enter a valid date.",
            dateISO: "Please enter a valid date (ISO).",
            number: "Please enter a valid number.",
            digits: "Please enter only digits.",
            creditcard: "Please enter a valid credit card number.",
            equalTo: "Please enter the same value again.",
            accept: "Please enter a value with a valid extension.",
            maxlength: $validatorProvider.format("Please enter no more than {0} characters."),
            minlength: $validatorProvider.format("Please enter at least {0} characters."),
            rangelength: $validatorProvider.format("Please enter a value between {0} and {1} characters long."),
            range: $validatorProvider.format("Please enter a value between {0} and {1}."),
            max: $validatorProvider.format("Please enter a value less than or equal to {0}."),
            min: $validatorProvider.format("Please enter a value greater than or equal to {0}.")
        });


    })
    .controller('MainController',['$rootScope','$scope',function ($rootScope,$scope) {
        $rootScope.title = 'ระบบการจัดการข้อมูล';
    }])
    .run(function () {
        /**
         * create prototype function
         */
        String.prototype.contains = function(keyword) {
            var self = angular.lowercase(this);
            var value = angular.lowercase(keyword);
            return self.indexOf(value) != -1;
        };

    })

;
function errorHandle(err) {
    console.log(err)
}