angular.module('orchid.ui.service',['ui.bootstrap','orchid.constants'])
    .service('CustomModalService',function ($uibModal,DirectoryConstants) {
        /**
         * @param config จัดการรายละเอียดการแสดงผลและข้อมูลภายใน
         * @returns {*} defer object
         */
        this.open = function (config,instance) {
            if ((!config.templateUrl) /*|| (!config.controller || !config.bindToController)*/) {
                alert('ระบบ template และ controller ทุครั้งที่เรียกใช้ custom modal');
                return;
            }
            var options = {};
            options.size = config.size || 'md';
            options.bindToController = true;
            options.controller = config.controller;
            options.templateUrl = DirectoryConstants.partials+config.templateUrl;
            options.windowClass  = 'modal-custom';
            options.scope = config.scope;
            options.resolve = {
                data:function () {
                    return config.data || {};
                }
            };
            var modalInstance = $uibModal.open(options);
            if (instance) return modalInstance;
            return modalInstance.result;
        };
    })
    .service('ConfirmModalService',function ($uibModal,DirectoryConstants) {
        this.open = function (message,iconClass) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: DirectoryConstants.partials+'confirmModal.jsp',
                controller : 'ConfirmModalController',
                size : 'sm',
                resolve : {
                    iconClass : function () {
                        return iconClass || 'pe-7s-check';
                    },
                    message : function () {
                        return message;
                    }
                },
                windowClass : 'modal-confirm'
            });

            return modalInstance.result;
        };
    })
    .service('NotificationMessage',function (DirectoryConstants) {
        /**
         *
         * @param obj
         * obj.class is color of alert message
         *  'info' : blue
         *  'success' : green
         *  'warning : yellow
         *  'danger' : red
         */

        this.show = function (obj) {
            if (!obj || !obj.message) {
                console.log('กรุณาใส่ข้อความของแจ้งเตือนทุกครั้งเวลาเรียกใช้.');
                return;
            }
            var alertMessage = obj.message;
            var alertColor = obj.className || 'info';
            var alertIcon = obj.icon || 'pe-7s-volume1';
            var alertTime = obj.time || 3;

            $.notify({
                icon: alertIcon,
                message: alertMessage

            },{
                type: alertColor,
                timer: alertTime*1000,
                placement: {
                    from: 'top',
                    align: 'center'
                }
            });
        };
    })
    .service('OnLoadModal',function ($uibModal,DirectoryConstants) {
        var modalInstance = undefined;
        this.open = function () {
            modalInstance = $uibModal.open({
                animation: true,
                templateUrl: DirectoryConstants.partials+'onLoadModal.jsp',
                size : 'sm',
                windowClass : 'modal-confirm',
                backdrop: 'static'
            });
        };

        this.close = function () {
            modalInstance.close();
        }
    })
    .controller('ConfirmModalController',['$scope','$uibModalInstance','message','iconClass',function ($scope,$uibModalInstance,message,iconClass) {
        $scope.message = message;
        $scope.iconClass = iconClass;
        $scope.ok = function () {
            $uibModalInstance.close(true);
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }])
;
