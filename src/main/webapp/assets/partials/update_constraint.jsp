<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="modal-header">
    <h3 class="modal-title" ng-bind="role.name"></h3>
</div>
<div class="modal-body">
    <button
            ng-repeat="constraint in role.constraints"
            class="btn btn-success margin-bottom col-md-6"
            ng-model="role.constraints[$index].haveConstraint"
            ng-class="{'btn-fill':role.constraints[$index].haveConstraint}"
            uib-btn-checkbox
            ng-bind="constraint.name"></button>

</div>
<div class="modal-footer">
    <button class="btn btn-primary" ng-click="ok()">บันทึกข้อมูล</button>
</div>
