<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<label for="profileImage" ng-bind="label"></label>
<label class="btn-file btn-primary">
    <input type="file" id="profileImage" ng-model="src"/>
    <span>เลือกรูปภาพ</span>
</label>
<span  ng-bind="src.name || 'ยังไม่ได้เลือกรูปภาพ'"></span>
<img class="img-preview" ng-src="{{image_source}}"  ng-show="image_source" />