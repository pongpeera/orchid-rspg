<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="modal-header">
    <h3 class="modal-title">เพิ่มข้อมูลต้นไม้</h3>
</div>
<div class="modal-body">
    <form ng-validate="addTreeValidation" name="addTreeForm">
        <div class="row">
            <div class="col-xs-6">
                <div class="form-group">
                    <label for="garden">สวนที่อยู่อาศัย : </label>
                    <select ng-model="tree.garden" class="form-control" id="garden">
                        <option ng-repeat="garden in gardens" ng-value="garden.id" ng-bind="garden.name"></option>
                    </select>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    <label for="species">สายพันธุ์ของต้นไม้ : </label>
                    <select ng-model="tree.species" class="form-control" id="species">
                        <option ng-repeat="species in treeSpecies" ng-value="species.id" ng-bind="species.name"></option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6">
                <div class="form-group">
                    <label for="latitude">ตำแหน่งละติจูดของต้นไม้ : </label>
                    <input type="text" class="form-control" id="latitude" ng-model="tree.latitude" name="latitude">
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    <label for="longitude">ตำแหน่งลองติจูดของต้นไม้ : </label>
                    <input type="text" class="form-control" id="longitude" ng-model="tree.longitude" name="longitude">
                </div>
            </div>
        </div>
    </form>

    <ng-map center="[40.74, -74.18]" on-click="placeMarker()" >
        <marker position="{{positionPicker}}" icon="{{greenPin}}"></marker>
        <marker
                ng-repeat="p in positions"
                position="{{p.lat}}, {{p.lng}}"
                icon="{{redPin}}"></marker>
    </ng-map>

</div>
<div class="modal-footer">
    <button class="btn btn-primary" ng-click="ok(addTreeForm)">บันทึกข้อมูล</button>
    <button class="btn btn-danger" ng-click="cancel()">ยกเลิก</button>
</div>