<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="form-group">
    <label for="keyword">กรอกคำค้นหาที่เกี่ยวข้อง</label>
    <input
            type="text" class="form-control" id="keyword"
            placeholder="กรอกคำค้นหาที่เกี่ยวข้อง"
            ng-model="keyword"
            ng-change="findByKeyword()"/>
</div>
<ul class="list-group">
    <li class="list-group-item" ng-if="suggestions.length > 0"
        ng-repeat="suggestion in suggestions track by $index"
        ng-class="{active : selectedIndex === $index}"
        ng-click="selectAndHide($index)"
        ng-bind="suggestion | autoCompleteItem"></li>
</ul>