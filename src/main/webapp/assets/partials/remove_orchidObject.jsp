<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="modal-header">
    <div class="pe-7s-close-circle"></div>
</div>
<div class="modal-body">
    <span>ต้องการลบข้อมูลต้นและข้อมูลเกี่ยวข้องกับกอกล้วยไม้หมายเลข {{ orchidObject.id}} หรือไม่</span>
    <label class="radio-inline"><input type="radio" ng-model="hasReason" ng-value="false" ng-checked="true"> มีข้อมูลผิดพลาดอยู่ในระบบ</label>
    <label class="radio-inline"><input type="radio" ng-model="hasReason" ng-value="true">มีการตาย</label>
    <div ng-show="hasReason">
        <div class="form-group">
            <label for="reason">สาเหตุการตาย : </label>
            <input type="text" class="form-control" id="reason" placeholder="กรุณากรอกคำอธิบาย" ng-model="orchidObject.reason">
        </div>
        <div class="form-group">
            <label for="removeDate">วันที่ตาย : </label>
            <input type="date" class="form-control" id="removeDate" ng-model="orchidObject.removeDate">
        </div>
    </div>

</div>
<div class="modal-footer">
    <button class="btn btn-primary" ng-click="ok()">ยืนยัน</button>
    <button class="btn btn-danger" ng-click="cancel()">ยกเลิก</button>
</div>
