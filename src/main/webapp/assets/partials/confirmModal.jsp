<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="modal-header">
    <div ng-class="iconClass"></div>
</div>
<div class="modal-body" ng-show="message">
    <span ng-bind="message"></span>
</div>
<div class="modal-footer">
    <button class="btn btn-primary" type="button" ng-click="ok()">ยืนยัน</button>
    <button class="btn btn-danger" type="button" ng-click="cancel()">ยกเลิก</button>
</div>