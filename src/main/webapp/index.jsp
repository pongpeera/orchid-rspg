<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tlds/rspg.tld" prefix="rspg"%>
<!DOCTYPE html>
<html lang="en" ng-app="orchid">
<head>
    <title ng-bind="$root.title"></title>
    <link href="./assets/components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <style>
        body{
            background-image: url("assets/img/index-bg-cover.jpg");
            color: #FFFFFF;
            background-size: cover  ;
            top: 0; left: 0;
            width: 100%; height: 100%;

        }
        body:before {
             content: '';
             position: absolute;
             top: 0;
             right: 0;
             bottom: 0;
             left: 0;
             background-image: linear-gradient(to bottom, #8c10cf, #b4a9cf);
             opacity: .9;

         }

        body .jumbotron  {
            background-color: initial;
            position: relative;

        }
        .input-group {
            width: 600px;
        }
        .vertical-center {
            min-height: 80%;

            display: flex;
            align-items: center;
        }
        .input-group-addon:last-child  {
            background-color: #ffffff !important;
            border-bottom-right-radius: 4px ;
            border-top-right-radius: 4px ;
        }
        .btn-default {
            border-radius: 4px !important;
            background-color: #9247cf;
            border: 2.5px #FFFFFF solid;
            color : #FFFFFF
        }

    </style>
</head>
<body ng-controller="MainController">
<img src="./assets/img/black-ribbon.png" style="left: 0;position: relative;" />
       <div class="jumbotron text-center  vertical-center">

           <div class="container container-fluid">
               <h1>โครงการพันธุ์กล้วยไม้พระราชทาน</h1>
               <p >...
               </p>
               <form class="form-inline">
                   <div class="form-group">
                       <div class="input-group  input-group-lg">
                           <input type="text" class="form-control"  placeholder="กรุณากรอกข้อความสำหรับการค้นหา" style="border-top-left-radius: 4px;border-bottom-left-radius: 4px;">
                           <div class="input-group-addon  dropdown-toggle">
                               ทั้งหมด<span class="caret"></span>
                           </div>
                       </div>
                   </div>
                   <button class="btn btn-default btn-lg">ค้นหาพันธุ์กล้วยไม้</button>
               </form>

            </div>
       </div>

    <div style="display: none;">
            <rspg:counter></rspg:counter>
    </div>
    <script src="./assets/components/jquery/dist/jquery.min.js"></script>
    <script src="./assets/components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="./assets/components/angular/angular.min.js"></script>
    <%--<script src="./assets/js/constants.js"></script>--%>
    <%--<script src="./assets/js/api.js"></script>--%>
    <%--<script src="./assets/js/login.js"></script>--%>
    <%--<script src="./assets/js/register.js"></script>--%>
    <%--<script src="./assets/js/services.js"></script>--%>
    <script src="./assets/js/app.js"></script>

</body>
</html>
