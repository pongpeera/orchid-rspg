
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="col-md-10 col-md-offset-1">
    <div class="row">
        <div class="col-md-4">
            <auto-complete array-object="userObject" selected-item="selectedUser"></auto-complete>
        </div>
    </div>
    <div class="row margin-top" ng-show="selectedUser">
        <div class="col-md-4">
            <div class="card card-user">
                <div class="image">
                    <%--<img src="https://ununsplash.imgix.net/photo-1431578500526-4d9613015464?fit=crop&fm=jpg&h=300&q=75&w=400" alt="..."/>--%>
                </div>
                <div class="content">
                    <div class="author">
                        <img class="avatar border-gray" src="../assets/img/orchid12.jpg" alt="..."/>
                        <h4 class="title" ng-bind="selectedUser.name"></h4>
                    </div>
                    <p ng-bind="selectedUser.email"></p>
                    <p ng-bind="selectedUser.telephone"></p>
                    <p ng-bind="selectedUser.department"></p>
                </div>
                <hr>
                <div class="text-center">
                    <button class="btn btn-danger" ng-click="confirmRemoveUser()">ลบผู้ใช้</button>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="row" ng-repeat="role in roles" style="font-size: 20px">
                <div
                    class="btn btn-primary margin-bottom"
                    ng-bind="role.name"
                    ng-class="{'btn-fill':roles[$index].wasRole}"
                    ng-model="roles[$index].wasRole"
                    uib-btn-checkbox
                    style="width:150px;"></div>
                <button class="btn margin-bottom" ng-disabled="!roles[$index].wasRole" ng-click="updateConstraint($index)">
                    <i class="pe-7s-plus"></i>
                </button>
            </div>
        </div>
        <button class="btn btn-primary" ng-click="assignRole()">บันทึกการเปลี่ยนแปลง</button>
    </div>
</div>