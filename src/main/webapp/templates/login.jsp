<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html ng-app="orchid.login">
<head>
    <title ng-bind="$root.title"></title>
    <link rel="stylesheet" href="../assets/components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/components/angular-bootstrap/ui-bootstrap-csp.css">
    <link rel="stylesheet" href="../assets/css/style.min.css">
</head>
<body >
    <div ng-controller="LoginController">
        <div class="container container-middle-page">
            <div class="row vertical-center-row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="panel panel-primary" >
                        <div class="panel-heading">ลงชื่อเข้าสู่ระบบ</div>
                        <div class="panel-body">
                            <form ng-validate="loginValidation" name="loginForm" ng-submit="verifyLogin($event,loginForm)">
                                <div class="row">
                                    <div class="col-md-10 col-md-offset-1">
                                        <div class="form-group">
                                            <label for="email">อีเมล</label>
                                            <input type="email" class="form-control" name="email" id="email" placeholder="example@mju.ac.th" ng-model="email">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-10 col-md-offset-1">
                                        <div class="form-group">
                                            <label for="password">รหัสผ่าน</label>
                                            <input type="password" class="form-control" name="password" id="password" placeholder="********" ng-model="password">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-10 col-md-offset-1" style="text-align: center">
                                        <button class="btn btn-primary" type="submit">เข้าสู่ระบบ</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="../assets/components/jquery/dist/jquery.min.js"></script>
    <script src="../assets/components/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="../assets/components/angular/angular.min.js"></script>
    <script src="../assets/components/angular-bootstrap/ui-bootstrap.min.js"></script>
    <script src="../assets/components/jpkleemans-angular-validate/dist/angular-validate.min.js"></script>
    <script src="../assets/components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../assets/components/light-bootstrap-dashboard/js/bootstrap-notify.js"></script>
    <script src="../assets/js/api.js"></script>
    <script src="../assets/js/constants.js"></script>
    <script src="../assets/js/services.js"></script>
    <script src="../assets/js/login.js"></script>
</body>
</html>
