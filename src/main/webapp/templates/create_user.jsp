<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="container container-middle-page">
    <div class="row vertical-center-row">
        <div class="col-md-9 col-md-offset-1">
            <div class="panel panel-info" >
                <div class="panel-heading">เพิ่มผู้ใช้งานใหม่</div>
                <div class="panel-body">
                    <form name="createUserForm" ng-submit="confirmAddUser($event,createUserForm)" ng-validate="createUserValidations">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="email">อีเมล</label>
                                    <input type="email" class="form-control" id="email" placeholder="example@mju.ac.th" ng-model="user.email" name="email">
                                </div>
                                <div class="form-group">
                                    <label for="password">รหัสผ่าน</label>
                                    <input type="password" class="form-control" id="password" placeholder="********" ng-model="user.password" name="password">
                                </div>
                                <div class="form-group">
                                    <label for="role">หน้าที่</label>
                                    <select class="form-control" ng-model="user.selectedRole" id="role" name="role">
                                        <option ng-repeat="role in roles" ng-bind="role.name" value="{{role.id}}"></option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <choose-image label="รูปประจำตัว" src="user.imageSrc" image_source="image_source"></choose-image>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name">ชื่อ - นามสกุล</label>
                                    <input type="text" class="form-control" id="name" placeholder="พงศ์พีระ คุณดิลกบริรักษ์" ng-model="user.name" name="name">
                                </div>
                                <div class="form-group">
                                    <label for="personalId">หมายเลขบัตรประชาชน</label>
                                    <input type="text" class="form-control" id="personalId" placeholder="1509901304xxxx" ng-model="user.personalId" name="personalId">
                                </div>
                                <div class="form-group">
                                    <label for="telephoneNumber">หมายเลขโทรศัพท์</label>
                                    <input type="text" class="form-control" id="telephoneNumber" placeholder="092-416-1xxx" ng-model="user.telephoneNumber" name="telephoneNumber">
                                </div>
                                <div class="form-group">
                                    <label for="department">หน่วยงานที่สังกัด</label>
                                    <input type="text" class="form-control" id="department" placeholder="คณะวิทยาศาสตร์ มหาวิทยาลัยแม่โจ้" ng-model="user.department" name="department">
                                </div>
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-md-2 col-md-offset-4">
                                <button class="btn btn-info btn-fill" style="font-size: 23px;">ยืนยันการเพิ่มผู้ใช้</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>