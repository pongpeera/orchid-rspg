
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html ng-app="orchid.register">
<head>
    <title ng-bind="$root.title"></title>
    <link rel="stylesheet" href="../assets/components/bootstrap/dist/css/bootstrap.min.css">
    <link href="../assets/components/light-bootstrap-dashboard/css/pe-icon-7-stroke.css" rel="stylesheet" />
    <link rel="stylesheet" href="../assets/components/angular-bootstrap/ui-bootstrap-csp.css">
    <link rel="stylesheet" href="../assets/css/style.min.css">
</head>
<body ng-controller="RegisterController">
    <div class="container">
        <div class="row  vertical-center-row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-primary" >
                    <div class="panel-heading">ลงทะเบียน</div>
                    <div class="panel-body">
                        <form ng-submit="onSubmit($event,form)">
                            <div class="form-group col-md-6">
                                <label for="email">email : </label>
                                <input type="email" class="form-control" id="email" ng-model="member.email">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="password">password : </label>
                                <input type="password" class="form-control" id="password" ng-model="member.password">
                            </div>
                            <div class="form-group col-md-6" >
                                <label for="name">name : </label>
                                <input type="text" class="form-control"  id="name" ng-model="member.name">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="telephoneNumber">telephone number : </label>
                                <input type="text" class="form-control" id="telephoneNumber" ng-model="member.telephoneNumber">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="postId">gender :</label>
                                <input type="radio" name="gender" ng-model="member.gender" value="0" checked> ชาย
                                <input type="radio" name="gender" ng-model="member.gender" value="1"> หญิง
                            </div>
                            <div class="form-group col-md-6">
                                <label for="birthDate">birth date :</label>
                                <input type="date" class="form-control" id="birthDate" ng-model="member.birthDate">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="address">address : </label>
                                <textarea id="address" class="form-control" ng-model="member.address"></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="personalId">personal id :</label>
                                <input type="text" class="form-control" id="personalId" ng-model="member.personalId">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="postId">post id :</label>
                                <input type="text" class="form-control" id="postId" ng-model="member.postId">
                            </div>
                            <div class="text-center col-md-12">
                                <button type="submit" class="btn btn-primary">ยืนยันการสมัคร</button>
                                <button type="reset" class="btn btn-danger">ยกเลิกข้อมูล</button>
                            </div>
                        </form>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        <form>

        </form>
    </div>

    <script src="../assets/components/jquery/dist/jquery.min.js"></script>
    <script src="../assets/components/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="../assets/components/angular/angular.min.js"></script>
    <script src="../assets/components/angular-bootstrap/ui-bootstrap.min.js"></script>
    <script src="../assets/components/light-bootstrap-dashboard/js/bootstrap-notify.js"></script>
    <script src="../assets/components/light-bootstrap-dashboard/js/light-bootstrap-dashboard.js"></script>
    <script src="../assets/components/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
    <script src="../assets/components/jpkleemans-angular-validate/dist/angular-validate.min.js"></script>
    <script src="../assets/components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../assets/js/api.js"></script>
    <script src="../assets/js/constants.js"></script>
    <script src="../assets/js/services.js"></script>
    <script src="../assets/js/register.js"></script>
</body>
</body>
</html>
