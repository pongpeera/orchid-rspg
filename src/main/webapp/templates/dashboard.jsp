<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html ng-app="orchid.backend">
<head>
    <meta charset="utf-8" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <title ng-bind="$root.title"></title>
    <link rel="stylesheet" href="../assets/components/bootstrap/dist/css/bootstrap.min.css">
    <link href="../assets/components/light-bootstrap-dashboard/css/animate.min.css" rel="stylesheet"/>
    <link href="../assets/components/light-bootstrap-dashboard/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <link href="../assets/components/light-bootstrap-dashboard/css/pe-icon-7-stroke.css" rel="stylesheet" />
    <link rel="stylesheet" href="../assets/components/angular-bootstrap/ui-bootstrap-csp.css">
    <link rel="stylesheet" href="../assets/css/style.min.css">
    <link rel="stylesheet" href="../assets/components/ng-table/dist/ng-table.css">
</head>
<body>
    <div ng-controller="ListMenuController">
          <div class="wrapper">
              <div class="sidebar" data-color="purple" data-image="../assets/img/sidebar-orchid.jpg">
                  <div class="sidebar-wrapper">
                      <div class="logo">
                          <a href="#" class="simple-text">
                              <img class="sidebar-profile-img" src="../assets/img/orchid12.jpg" alt="...">
                              <h4 class="title">
                                  พงศ์พีระ คุณดิลกบริรักษ์
                                  <h6>ผู้ดูแลระบบ</h6>
                              </h4>
                          </a>
                      </div>
                      <ul class="nav">
                          <li <%--ng-class="{'active':isActive('')}"--%>>
                              <a href="#">
                                  <i class="pe-7s-graph"></i>
                                  <p class="nav-bar-font">กระดานหลัก</p>
                              </a>
                          </li>
                          <li ng-class="{'active':isActive('/create_user')}">
                              <a href="#create_user">
                                  <i class="pe-7s-user"></i>
                                  <p class="nav-bar-font">เพิ่มผู้ใช้ใหม่</p>
                              </a>
                          </li>
                          <li ng-class="{'active':isActive('/manage_user')}">
                              <a href="#manage_user">
                                  <i class="pe-7s-note2"></i>
                                  <p class="nav-bar-font">จัดการข้อมูลผู้ใช้</p>
                              </a>
                          </li>
                          <li ng-class="{'active':isActive('/staff/tree')}">
                              <a href="#staff/tree">
                                  <i class="pe-7s-leaf"></i>
                                  <p class="nav-bar-font">จัดการข้อมูลต้นไม้</p>
                              </a>
                          </li>
                          <li ng-class="{'active':isActive('/staff/orchid')}">
                              <a href="#staff/orchid">
                                  <i class="pe-7s-science"></i>
                                  <p class="nav-bar-font">จัดการข้อมูลพันธุ์กล้วยไม้</p>
                              </a>
                          </li>
                          <li ng-class="{'active':isActive('/staff/object')}">
                              <a href="#staff/object">
                                  <i class="pe-7s-settings"></i>
                                  <p class="nav-bar-font">จัดการข้อมูลกล้วยไม้</p>
                              </a>
                          </li>
                          <li ng-class="{'active':isActive('/staff/removelog')}">
                              <a href="#staff/removelog">
                                  <i class="pe-7s-settings"></i>
                                  <p class="nav-bar-font">ประวัติการลบข้อมูล</p>
                              </a>
                          </li>
                      </ul>
                  </div>
              </div>
              <div class="main-panel">
                  <nav class="navbar navbar-default navbar-fixed">
                      <div class="container-fluid">
                          <div class="navbar-header">
                              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                                  <span class="sr-only">Toggle navigation</span>
                                  <span class="icon-bar"></span>
                                  <span class="icon-bar"></span>
                                  <span class="icon-bar"></span>
                              </button>
                              <p class="navbar-brand" href="#" ng-bind="contentTitle"></p>
                          </div>
                          <ul class="nav navbar-nav navbar-right">
                              <li >
                                  <a href="#" ng-click="onLogout()">ออกจากระบบ</a>
                              </li>
                          </ul>
                      </div>

                  </nav>

                  <div class="content">
                      <div ng-view></div>
                  </div>
                  <footer class="footer">
                      <div class="container-fluid">
                      </div>
                  </footer>
              </div>
          </div>

    </div>
    <script src="../assets/components/jquery/dist/jquery.min.js"></script>
    <script src="../assets/components/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="../assets/components/angular/angular.min.js"></script>
    <script src="../assets/components/angular-route/angular-route.min.js"></script>
    <script src="../assets/components/angular-animate/angular-animate.min.js"></script>
    <script src="../assets/components/angular-bootstrap/ui-bootstrap.min.js"></script>
    <script src="../assets/components/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
    <script src="../assets/components/ng-table/dist/ng-table.min.js"></script>
    <script src="../assets/components/jpkleemans-angular-validate/dist/angular-validate.min.js"></script>
    <script src="../assets/components/moment/moment.js"></script>
    <script src="../assets/components/moment/locale/th.js"></script>
    <script src="../assets/components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../assets/components/light-bootstrap-dashboard/js/bootstrap-checkbox-radio-switch.js"></script>
    <script src="../assets/components/light-bootstrap-dashboard/js/chartist.min.js"></script>
    <script src="../assets/components/light-bootstrap-dashboard/js/bootstrap-notify.js"></script>
    <script src="../assets/components/light-bootstrap-dashboard/js/light-bootstrap-dashboard.js"></script>
    <script src="../assets/components/ngmap/build/scripts/ng-map.min.js"></script>
    <script src="../assets/components/lodash/dist/lodash.min.js"></script>
    <script src="http://maps.google.com/maps/api/js?key=AIzaSyAqyDO58pYGnbLgofgilqgAm2Rs5FYzLuA"></script>
    <script src="../assets/js/constants.js"></script>
    <script src="../assets/js/api.js"></script>
    <script src="../assets/js/services.js"></script>
    <script src="../assets/js/filters.js"></script>
    <script src="../assets/js/directives.js"></script>
    <script src="../assets/js/users.js"></script>
    <script src="../assets/js/login.js"></script>
    <script src="../assets/js/orchid.js"></script>
    <script src="../assets/js/garden.js"></script>
    <script src="../assets/js/tree.js"></script>
    <script src="../assets/js/orchidObject.js"></script>
    <script src="../assets/js/log.js"></script>
    <script src="../assets/js/dashboard.js"></script>
    <script src="../assets/js/backend.js"></script>
</body>
</html>
