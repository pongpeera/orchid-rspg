<%--
  Created by IntelliJ IDEA.
  User: The Exiled
  Date: 11/8/2559
  Time: 14:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="col-md-offset-4 col-md-4">
    <div class="card">
        <div class="image card-image-header">
            <img src="https://ununsplash.imgix.net/photo-1431578500526-4d9613015464?fit=crop&fm=jpg&h=300&q=75&w=400" alt="">
        </div>
        <div class="content">
            <form name="addOrchidObjectForm" ng-validate="addOrchidObjectValidation">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="treeId">รหัสต้นไม้ที่อยู่อาศัย</label>
                        <input type="text" class="form-control" id="treeId"  ng-model="orchidObject.tree.id" ng-disabled="true">
                    </div>
                    <div class="form-group">
                        <label for="treeName">ชื่อต้นไม้ที่อยู่อาศัย</label>
                        <input type="text" class="form-control" id="treeName"  ng-model="orchidObject.tree.name" ng-disabled="true">
                    </div>
                    <div class="form-group">
                        <label for="treeGarden">ชื่อต้นไม้ที่อยู่อาศัย</label>
                        <input type="text" class="form-control" id="treeGarden"  ng-model="orchidObject.tree.garden" ng-disabled="true">
                    </div>
                    <div class="form-group">
                        <label for="id">หมายเลขกล้วยไม้</label>
                        <input type="text" class="form-control" id="id" placeholder="1" ng-model="orchidObject.id" name="id">
                    </div>
                    <div class="form-group">
                        <label for="orchidSpecies">สายพันธฺุกล้วยไม้</label>
                        <select class="form-control" id="orchidSpecies" ng-model="orchidObject.species">
                            <option ng-repeat="orchid in orchidSpecies" ng-value="orchid.id" ng-bind="orchid.localName"></option>
                        </select>
                    </div>
                </div>
            </form>
        </div>
        <div class="footer">
            <button class="btn btn-primary" ng-click="addOrchidObject(addOrchidObjectForm)">เพิ่มกล้วยไม้ใหม่</button>
        </div>
   </div>
</div>


