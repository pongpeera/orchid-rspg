<%--
  Created by IntelliJ IDEA.
  User: The Exiled
  Date: 10/8/2559
  Time: 13:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="col-md-offset-2 col-md-8">
    <div class="card">
        <div class="image card-image-header">
            <img src="https://ununsplash.imgix.net/photo-1431578500526-4d9613015464?fit=crop&fm=jpg&h=300&q=75&w=400" alt="">
        </div>
        <div class="content">
            <form ng-validate="editTreeValidation" name="editTreeForm">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="garden">สวนที่อยู่อาศัย : </label>
                            <select ng-model="tree.garden.id" class="form-control" id="garden">
                                <option ng-repeat="garden in gardens" value="{{garden.id}}" ng-bind="garden.name" ></option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="species">สายพันธุ์ของต้นไม้ : </label>
                            <select ng-model="tree.species.id" class="form-control" id="species">
                                <option ng-repeat="species in treeSpecies" value="{{species.id}}" ng-bind="species.name" ></option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="latitude">ตำแหน่งละติจูดของต้นไม้ : </label>
                            <input type="text" class="form-control" id="latitude" ng-model="tree.latitude" name="latitude">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="longitude">ตำแหน่งลองติจูดของต้นไม้ : </label>
                            <input type="text" class="form-control" id="longitude" ng-model="tree.longitude" name="longitude">
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="footer">
            <button class="btn btn-primary" ng-click="updateTree(editTreeForm)">บันทึกข้อมูล</button>
        </div>
    </div>
</div>


