<%--
  Created by IntelliJ IDEA.
  User: The Exiled
  Date: 9/8/2559
  Time: 22:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="col-md-offset-2 col-md-8">
    <div class="card">
        <div class="image card-image-header">
            <img src="https://ununsplash.imgix.net/photo-1431578500526-4d9613015464?fit=crop&fm=jpg&h=300&q=75&w=400" alt="">
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="id">หมายเลขทางสายพันธุ์</label>
                        <input type="text" class="form-control" id="id" ng-model="orchidDetails.id" ng-disabled="true">
                    </div>
                    <div class="form-group">
                        <label for="nameOfScience">ชื่อทางวิทยาศาสตร์</label>
                        <input type="text" class="form-control" id="nameOfScience" ng-model="orchidDetails.nameOfScience">
                    </div>
                    <div class="form-group">
                        <label for="id">ชื่อทางท้องถิ่น</label>
                        <input type="text" class="form-control" id="localName" ng-model="orchidDetails.localName">
                    </div>
                    <label class="checkbox-inline" ng-repeat="b in benefit">
                        <input type="checkbox" ng-model="b.selected" ng-value="b.id" ng-change="selectBenefit()"> {{b.name}}
                    </label>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="monthOfFlower">ระยะออกดอก</label>
                        <select class="form-control" id="monthOfFlower" ng-model="orchidDetails.flower">
                            <option ng-repeat="month in months" value="{{month}}" ng-bind="month"></option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="monthOfEndFlower">ระยะสิ้นสุดการออกดอก</label>
                        <select class="form-control" id="monthOfEndFlower" ng-model="orchidDetails.endOfFlower">
                            <option ng-repeat="month in months" value="{{month}}" ng-bind="month"></option>
                        </select>
                    </div>
                    <label class="checkbox-inline" ng-repeat="s in status">
                        <input type="checkbox"  ng-value="s.id" ng-model="s.selected" ng-change="selectStatus()"> {{s.name}}
                    </label>
                </div>
            </div>
        </div>
        <div class="footer">
            <button class="btn btn-primary" ng-click="updateOrchid()">แก้ไข</button>
        </div>
     </div>
</div>
