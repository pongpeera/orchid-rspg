<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="col-md-12  card-board">
    <div class="col-md-12">
        <button class="btn btn-success" ng-click="fetchRemoveLog()">
            อัปเดตข้อมูล
        </button>
    </div>
    <div class="col-md-12">
        <label class="radio-inline"><input type="radio" ng-model="logType" ng-value="'TreeRemoveLog'" ng-checked="true"> ประวัติการตายของต้นไม้</label>
        <label class="radio-inline"><input type="radio" ng-model="logType" ng-value="'OrchidObjectRemoveLog'"> ประวัติการตายของกอกล้วยไม้</label>
    </div>
    <div ng-if="logType == 'TreeRemoveLog'">
        <table ng-table="treeLogTableParams" class="table table-hover" show-filter="true">
            <tr ng-repeat="log in $data">
                <td title="'วันที่'" >
                    <span ng-bind="log.removeDate | removeDateFormat"></span>
                </td>
                <td title="'รหัสต้นไม้'" filter="{ treeId : 'number'}" sortable="'treeId'">
                    <span ng-bind="log.treeId"></span>
                </td>
                <%--<td title="'ชื่อต้นไม้'" filter="{ treeName : 'text'}" sortable="'treeName'">
                    <span ng-bind="log.treeName"></span>
                </td>
                <td title="'สถานที่'" filter="{ treeGarden : 'text'}" sortable="'treeGarden'">
                    <span ng-bind="log.treeGarden"></span>
                </td>--%>
                <td title="'สาเหตุการตายหรือย้ายออก'" filter="{ description : 'text' }" sortable="'description'">
                    <span ng-bind="log.description"></span>
                </td>
            </tr>
        </table>
    </div>
    <div ng-if="logType == 'OrchidObjectRemoveLog'">
        <table ng-table="orchidObjectLogTableParams" class="table table-hover" show-filter="true">
            <tr ng-repeat="log in $data">
                <td title="'วันที่'">
                    <span ng-bind="log.removeDate | removeDateFormat"></span>
                </td>
                <td title="'ชื่อพันธุ์กล้วยไม้'" filter="{ orchidName : 'text' }" sortable="'orchidName'">
                    <span ng-bind="log.orchidName"></span>
                </td>
                <td title="'รหัสกอกล้วยไม้'" filter="{ orchidObjectId : 'text' }" sortable="'orchidObjectId'">
                    <span ng-bind="log.orchidObjectId"></span>
                </td>
                <td title="'สาเหตุการตายหรือย้ายออก'" filter="{ description : 'text' }" sortable="'description'">
                    <span ng-bind="log.description"></span>
                </td>
            </tr>
        </table>
    </div>
</div>
