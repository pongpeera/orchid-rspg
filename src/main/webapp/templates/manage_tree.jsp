
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="col-md-12  card-board">
    <div class="col-md-12">
        <button class="btn btn-success" ng-click="onModalOpen()">
            เพิ่มต้นไม้ใหม่
        </button>
    </div>
    <div class="col-md-12">
        <table ng-table="tableParams" class="table table-hover" show-filter="false">
            <tr ng-repeat="tree in $data">
                <td title="'รหัสต้นไม้'" filter="{ id: 'number'}" sortable="'id'">
                    <span ng-bind="tree.id"></span>
                </td>
                <td title="'สายพันธุ์'" filter="{ species : 'text'}" sortable="'species'">
                    <span ng-bind="tree.species"></span>
                </td>
                <td title="'สานที่ตั้ง'" filter="{ garden : 'text'}" sortable="'garden'">
                    <span ng-bind="tree.garden"></span>
                </td>
         <%--       <td title="'โซน'" filter="{ zone : 'text'}" sortable="'zone'">
                    <span ng-bind="tree.zone"></span>
                </td>--%>
                <td title="'รายละเอียด'">
                    <a href="#staff/tree/{{tree.id}}" class="btn btn-warning">รายละเอียด</a>
                </td>
                <td title="'เพิ่มกอกล้วยไม้'">
                    <a href="#staff/object/{{tree.id}}/add" class="btn btn-success">เพิ่มกอกล้วยไม้</a>
                </td>
                <td title="'แก้ไข'">
                    <a href="#staff/tree/{{tree.id}}/edit" class="btn btn-warning">แก้ไขข้อมูล</a>
                </td>
                <td title="'ลบ'">
                    <button class="btn btn-danger" ng-click="confirmRemoveTree(tree)">ลบข้อมูล</button>
                </td>
            </tr>
        </table>
    </div>
</div>


