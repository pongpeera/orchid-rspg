<%--
  Created by IntelliJ IDEA.
  User: The Exiled
  Date: 10/8/2559
  Time: 22:26j
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="col-md-12 card-board">
    <div class="row">
        <div class="col-md-4 col-md-offset-1">
            <img ng-src="../assets/img/orchid{{orchidObjectDetails.orchid.id}}.jpg" alt="" class="img-details">
        </div>
        <div class="col-md-3">
            <p><strong>หมายเลขกล้วยไม้ : </strong><span ng-bind="orchidObjectDetails.id"></span></p>
            <p><strong>พันธุ์กล้วยไม้ : </strong><span ng-bind="orchidObjectDetails.orchid.localName"></span></p>
            <p><strong>ชื่อต้นไม้ : </strong><span ng-bind="orchidObjectDetails.tree.name"></span></p>
            <p><strong>อุทยาน : </strong><span ng-bind="orchidObjectDetails.tree.garden.name"></span></p>
           <%-- <p><strong>ประจำพื้นที่ : </strong><span ng-bind="orchidObjectDetails.zone"></span></p>--%>
        </div>
    </div>
    <div class="row">

    </div>
</div>