<%--
  Created by IntelliJ IDEA.
  User: The Exiled
  Date: 6/8/2559
  Time: 11:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="col-md-12">
    <div class="col-md-4">
        <auto-complete array-object="orchids" selected-item="selectedOrchid"></auto-complete>
    </div>
    <div class="col-md-2 col-md-offset-6">
        <a class="btn btn-success" href="#staff/orchid/add">เพิ่มกล้วยไม้ใหม่</a>
    </div>
</div>
<div class="col-md-12 card-board margin-top" ng-show="selectedOrchid">
    <div class="col-md-4 col-md-offset-1">
        <img ng-src="../assets/img/orchid{{selectedOrchid.id}}.jpg" alt="" class="img-details">
    </div>
    <div class="col-md-6">
        <p><strong>หมายเลขกล้วยไม้ : </strong><span ng-bind="selectedOrchid.id"></span></p>
        <p><strong>พันธุ์กล้วยไม้ชื่อทางวิทยาศาสตร์ : </strong><span ng-bind="selectedOrchid.nameOfScience"></span></p>
        <p><strong>พันธุ์กล้วยไม้ชื่อท้องถิ่น : </strong><span ng-bind="selectedOrchid.localName"></span></p>
        <a href="#staff/orchid/{{selectedOrchid.id}}/edit" class="btn btn-default">แก้ไข</a>
        <button class="btn btn-danger" ng-click="confirmRemoveOrchid()">ลบ</button>
    </div>
</div>
