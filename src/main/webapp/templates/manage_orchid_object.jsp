<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="col-md-12  card-board">
    <table ng-table="tableParams" class="table table-hover" show-filter="false">
        <tr ng-repeat="orchidObject in $data">
            <td title="'รหัสกอกล้วยไม้'" filter="{ id: 'number'}" sortable="'id'">
                <span ng-bind="orchidObject.id"></span>
            </td>
            <td title="'ชื่อท้องถิ่น'" filter="{ localName : 'text'}" sortable="'localName'">
                <span ng-bind="orchidObject.orchid.localName"></span>
            </td>
            <td title="'ต้นไม้ที่อยู่อาศัย'" filter="{ tree : 'text'}" sortable="'tree'">
                <span ng-bind="orchidObject.tree.id"></span>
            </td>
        <%--    <td title="'โซน'" filter="{ zone : 'text'}" sortable="'zone'">
                <span ng-bind="orchidObject.zone"></span>
            </td>--%>
            <td title="'สถานที่ตั้ง'" filter="{ garden : 'text'}" sortable="'garden'">
                <span ng-bind="orchidObject.tree.garden.name"></span>
            </td>
            <td title="'รายละเอียด'">
                <a href="#staff/object/{{orchidObject.id}}" class="btn btn-warning">รายละเอียด</a>
            </td>
            <td title="'แก้ไข'">
                <a href="#staff/object/{{orchidObject.id}}/edit" class="btn btn-warning">แก้ไขข้อมูลสายพันธุ์</a>
            </td>
            <td title="'ลบ'">
                <button class="btn btn-danger" ng-click="confirmRemoveOrchidObject(orchidObject)">ลบข้อมูล</button>
            </td>
        </tr>
    </table>
</div>
