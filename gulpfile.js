/**
 * Created by The Exiled on 9/8/2559.
 */
'use strict';
var gulp = require("gulp");
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var clean = require('gulp-clean');
var cleanCSS = require('gulp-clean-css');

var assets = './src/main/webapp/assets/';
var path = {
    sass : assets+'sass/*.scss',
    css : assets+'css/*.css',
    sass_partial :assets+'cass/_*.scss'
};

gulp.task('default', ['css-minify'], function () {
    gulp.watch([path.sass,path.sass_partial],['css-minify']);
});

gulp.task('css-clean', function () {
    return gulp.src(path.css)
        .pipe(clean({force: true}));
});

gulp.task('sass', function () {
    return gulp.src(path.sass)
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(assets+'css'));
});

gulp.task('css-minify',['css-clean','sass'], function() {
    return gulp.src(path.css)
        .pipe(concat('style.css'))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(assets+'css'));
});